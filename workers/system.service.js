/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Служебный сервис, для внутренних действий
 */

require('../_constants');
const {exec} = require('child_process');
const {shell} = require('electron');
const IPC = require('../server/ipc');
// const Database = require('../server/db');

IPC.on('open:link', (event, link) => {
  if (
    APP_SETTING.hasOwnProperty('sysSoft') &&
    String(APP_SETTING.sysSoft.browser).length > 0 &&
    String(APP_SETTING.sysSoft.browser).includes(':')
  ) {
    exec([APP_SETTING.sysSoft.browser, link].join(' '));
  } else {
    shell.openExternal(link);
  }
});

// IPC.on('cron:getList', (event, data) => {
// });
