/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Сервис вывод уведомлений в UI
 */

require('../_constants');
const {exec} = require('child_process');
const crontab = require('node-crontab'); // https://www.npmjs.com/package/node-crontab
const IPC = require('../server/ipc');
const Database = require('../server/db');

let tasks = {};

class Task {
  constructor(item) {
    this._item = item;
    this._jobId = null;
  }

  run() {
    this._jobId = crontab.scheduleJob(this._item.genetated, () => {
      this.launch(this._item.cmd);
    });
  }

  launch(cmd) { // ручной запуск задачи из UI
    exec(cmd || this._item.cmd, {
      maxBuffer: 1e+9,
      windowsHide: true,
      env: process.env
    }, (error, stdout, stderr) => {
      if (error) {
        console.error(error);
      }
    });
  }

  stop() {
    crontab.cancelJob(this._jobId);
  }

  destroy() {
    this.stop();
    delete tasks[`t${this._item.id}`];
  }
}

IPC.on('cron:getList', (event, data) => {
  Database.Cron.findAll({raw: true}).then(list => {
    event.sender.send(`cron:getList`, list.map(item => {
      item.time = JSON.parse(item.time);
      if (!tasks[`t${item.id}`] && item.active && String(item.cmd).length > 0) {
        tasks[`t${item.id}`] = new Task(item);
        tasks[`t${item.id}`].run();
      }
      return item;
    }));
  })
});

IPC.on('cron:save', (event, data) => {
  let item;
  if (data.id) {
    item = Database.Cron.update(data, {where: {id: data.id}});
  } else {
    item = Database.Cron.create(data);
  }

  item.then(() => {
    event.sender.send(`cron:save:ok`);
  }).catch(e => {
    console.error(e);
  });
});

IPC.on('cron:remove', (event, id) => {
  Database.Cron.destroy({where: {id}}).then(() => {
    event.sender.send(`cron:remove:ok`);
    tasks[`t${id}`].destroy();
  });
});

IPC.on('cron:setState', (event, data) => {
  Database.Cron.update({active: data.active}, {where: {id: data.id}});
  if (!data.active && tasks[`t${data.id}`]) {
    tasks[`t${data.id}`].stop();
  }
});

IPC.on('cron:run', (event, data) => {
  tasks[`t${data.id}`].launch();
});
