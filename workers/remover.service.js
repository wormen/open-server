/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Сервис для автоматического удаления старых бэкапов
 */

require('../_constants');
const fs = require('fs');
const {exec} = require('child_process');
const queue = require('async/queue');
const Fs = require('../server/libs/Fs');
const {getKey} = require('../server/libs/os');

let DIR = null;
let days = 1;

const REMOVE_QUEUE = queue(async (filePath, done) => {
  if (fs.existsSync(filePath)) {
    await Fs.remove(filePath);
  }
  done();
}, 3);

const oldList = (params = {}) => {
  params = Object.assign({days: 5, dir: ''}, params);
  return new Promise((resolve, reject) => {
    let cmd = `forfiles /p ${params.dir} /m * /s /d -${params.days} /c "cmd /c echo @path"`;
    try {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          return reject(err);
        }
        resolve(stdout.trim().replace(/"/g, '').split(`\n`).map(item => {
          return item.trim();
        }));
      });
    } catch (e) {
      reject(e);
    }
  });
};

const init = async (dir) => {
  // получаем настройки
  await getKey('sys').then(val => {
    if (val && typeof val.liveTime === 'number') {
      days = val.liveTime;
    }
  });

  oldList({days, dir}).then(data => {
    data.forEach(item => {
      REMOVE_QUEUE.push(item);
    });
  });

  setTimeout(() => {
    init(dir);
  }, 3 * 60e3);
};

const checkPath = () => {
  DIR = process.env.OPEN_SERVER.BACKUP_DIR;
  if (fs.existsSync(DIR) && !DIR.includes($app.cwd)) {
    init(DIR);
  } else {
    setTimeout(checkPath, 300);
  }
};
checkPath();
