/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Сервис для создания и восстановления бэкапов
 */

require('../_constants');

const Queue = require('../server/mqtt');
const Log = require('../server/libs/Log');
const Core = require('./Core');

const queue = new Queue();

queue.consume(Queue.EVENTS.BACKUP, async (msg) => {
  try {
    switch (msg.type) {
      case 'files':
        Core.backupFiles(msg);
        break;
      case 'mysql':
        Core.backupMySQL(msg);
        break
    }
    Log.info('create backup', JSON.stringify(msg));
  } catch (e) {
    console.error(e);
  }
});

queue.consume(Queue.EVENTS.RESTORE_BACKUP, async (msg) => {
  try {
    switch (msg.type) {
      case 'files':
        Core.restoreFiles(msg);
        break;
      case 'mysql':
        Core.restoreMySQL(msg);
        break
    }
    Log.info('restore backup', JSON.stringify(msg));
  } catch (e) {
    console.error(e);
  }
});
