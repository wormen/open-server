/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Основной функционал для работы сервисов
 */

const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const {spawn, exec} = require('child_process');
const Backup = require('../server/libs/backup');
const OpenServer = require('../server/libs/os');
const Fs = require('../server/libs/Fs');
const Log = require('../server/libs/Log');

const {getKey} = OpenServer;
let dest = null;
let src = null;
let user = 'root';
let pass = '';

class Core {
  static async backupFiles(msg) {
    src = path.resolve(msg.root, msg.dir);
    await getKey('sys').then(async (val) => {
      if (val && val.hasOwnProperty('backupDir') && String(val.backupDir).length >= 2) {
        await Fs.emptyDir(val.backupDir);
        dest = path.resolve(val.backupDir, (new Backup.nameItem(msg.dir, msg)).generate());
      } else {
        // todo запись в БД об невалидном пути
      }
    });

    if (dest) {
      (new Backup.Files.Pack(src, dest)).on('done', () => {
        // todo пишем в БД историю
      });
    }
  }

  static async restoreFiles(msg) {
    await getKey('sys').then(async (val) => {
      if (val && val.hasOwnProperty('backupDir')) {
        src = path.resolve(val.backupDir, msg.archive);
      }
    });

    await getKey('OS').then(async (val) => {
      if (_.isObject(val) && val.hasOwnProperty('www') && String(val.www).length >= 2) {
        dest = path.resolve(val.www, msg.name);
      } else {
        // todo запись в БД об невалидном пути
      }
    });

    if (dest && !fs.existsSync(dest)) {
      await Fs.emptyDir(dest);
    } else {
      // todo запись в БД об невалидном пути
    }

    if (src && fs.existsSync(src) && fs.existsSync(dest)) {
      (new Backup.Files.Unpack(src, dest)).on('done', () => {
        // todo пишем в БД историю
      });
    }
  }

  static _getSetting() {
    return Promise.all([
      getKey('server'),
      getKey('sys')
    ]).then(([server, sys]) => {
      pass = server.mysql.rootPassword;
      let mysql = process.env.OPEN_SERVER.ACTIVE_DRIVER.MYSQL;
      return {
        server, sys, pass,
        driver: mysql,
        driverDir: path.resolve((new OpenServer()).modulesDir, 'database', mysql, 'bin')
      }
    })
  }

  static async backupMySQL(msg) {
    this._getSetting().then(({server, sys, pass, driver, driverDir}) => {
      try {
        dest = path.resolve(sys.backupDir, (new Backup.nameItem(msg.name, msg)).generate());
        pass = String(pass).length === 0 ? '' : ` -p${pass}`;

        let exe = path.resolve(driverDir, 'mysqldump.exe');
        exec(`${exe} -u ${user}${pass} ${msg.name} > ${dest}`, (error, stdout, stderr) => {
          if (error) {
            return Log.error(error);
          }
          if (stderr) {
            return Log.error('stderr -->', stderr);
          }
          Log.debug(stdout);
        });
      } catch (e) {
        console.error(e);
      }
    }).catch(Log.error);
  }

  static async restoreMySQL(msg) {
    this._getSetting().then(({server, sys, pass, driver, driverDir}) => {
      try {
        src = path.resolve(sys.backupDir, msg.archive);
        pass = String(pass).length === 0 ? '' : ` -p${pass}`;

        let exe = path.resolve(driverDir, 'mysql.exe');
        exec(`${exe} -u ${user}${pass} ${msg.name} < ${src}`, (error, stdout, stderr) => {
          if (error) {
            return console.error(error);
          }
          if (stderr) {
            return console.error('stderr -->', stderr);
          }
          Log.debug(stdout);
        });
      } catch (e) {
        console.error(e);
      }
    }).catch(console.error);
  }
}

module.exports = Core;
