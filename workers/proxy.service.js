/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Proxy server
 */

require('../_constants');
const IPC = require('../server/ipc');
const Proxy = require('../server/proxy');
const Log = require('../server/libs/Log');

let setting = { // настройки по умолчанию
  opts: {
    autoOn: false,
    logOn: true,
    port: 9100
  }
};

(async () => {
  // зачитыаем настройки
  const proxy = new Proxy({port: setting.opts.port});

  proxy.createServer();
  proxy.on('request', (args) => {
    // if (setting.opts.logOn) {
    console.log(`[Proxy ${String(args.protocol).toUpperCase()}]`, args.method, args.url, args);
    // queue.addToQueue(Queue.EVENTS.PROXY, args);
    // }
    try {
      $Faye.sendClient('proxy-log', args);
    } catch (e) {
      console.error(e)
    }
  });

  // proxy.on('replace:url', (url, done) => {
  //   if (url.includes('cdn.shop-engine.ru')) {
  //     url = url.replace('cdn.shop-engine.ru', 'server.loc:3602/cdn-store')
  //   }
  //
  //   done(url);
  // });

  IPC.on('save-setting', (event, val) => {
    if (Number(setting.opts.port) !== Number(val.settings.server.proxy.port)) {
      Log.info('Proxy restarting...');
      proxy.restart(val.server.proxy, () => {
        Log.info('Proxy restarted');
      });
    }
    setting.opts = val.settings.server.proxy;
  });

  module.exports.setSetting = (params) => {
    if (params && params.ignore) {
      proxy.setIgnore(params.ignore);
    }
  };
})();
