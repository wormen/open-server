/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Сервис вывод уведомлений в UI
 */

require('../_constants');

const Queue = require('../server/mqtt');
const queue = new Queue();

queue.consume(Queue.EVENTS.NOTIFY, (msg) => {
  $Faye.sendClient('notify', msg);
});
