/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Сервис вывод уведомлений в UI
 */

require('../_constants');

const os = require('os');
const fs = require('fs');
const path = require('path');
const IPC = require('../server/ipc');
const Fs = require('../server/libs/Fs');

let FILE;
IPC.on('load-env', async (event, name) => {
  FILE = path.resolve(DATA_DIR, `${name}.env`);

  if (fs.existsSync(FILE)) {
    Fs.readFile(FILE).then(data => {
      if (name === 'path') {
        data = String(data).trim().split(';').map(line => {
          return {key: '', value: line};
        });
      } else {
        data = parseEnv(data);
      }
      event.sender.send(`load-env::data:${name}`, data);
    });
  }
});

IPC.on('save-env', (event, {name, data}) => {
  let list = [];
  for (let {key, value} of data) {
    if (name === 'path') {
      if (String(value).length > 0) {
        list.push(value);
      }
    } else {
      let envObj = {};
      if (String(key).length > 0) {
        list.push([key, value].join('='));
        envObj[key] = value;
      }
      if (Object.keys(envObj).length > 0) {
        mergeEnv(envObj);
      }
    }
  }

  if (process.env.OPEN_SERVER.VALID_ROOT_DIR && name === 'path') {
    let dir = (new (require('../server/libs/os'))()).userdataPath;
    Fs.writeFile(path.resolve(dir, 'config', `path.txt`), list.join(os.EOL) + os.EOL);
  }
  Fs.writeFile(path.resolve(DATA_DIR, `${name}.env`), list.join(name === 'path' ? ';' : os.EOL));
});

FILE = path.resolve(DATA_DIR, `user.env`);
if (fs.existsSync(FILE)) {
  Fs.readFile(FILE).then(parseEnv);
}

function parseEnv(data) {
  let list = [];
  let parseValue = v => {
    v = v.toString()
      .trim()
      .replace(/(^['"]|["']$)/g, '')
      .replace(/(^["]|["]$)/g, '')
      .trim();
    return v;
  };
  let envObj = {};
  data.toString().split(os.EOL).forEach(line => {
    let keyValueArr = line.match(/^\s*([\w\.\-]+)\s*=\s*(.*)?\s*$/);
    if (keyValueArr != null) {
      let key = keyValueArr[1];
      let value = parseValue(keyValueArr[2]) || '';
      list.push({key, value});
      envObj[key] = value;
    }
  });
  if (Object.keys(envObj).length > 0) {
    mergeEnv(envObj);
  }
  return list;
}
