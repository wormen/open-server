/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Сервис мониторинга логов и вывод их в UI
 */

require('../_constants');

const fs = require('fs');
const path = require('path');
const Log = require('../server/libs/Log');
const Fs = require('../server/libs/Fs');
const Queue = require('../server/mqtt');
const WC = require('../server/worker-core');
const {FileWatcher, LogWacher} = WC;
const OpenServer = require('../server/libs/os');

const queue = new Queue();
const fw = new FileWatcher();

let watchDir = null;
let channels = [];
let monFiles = [];

const initChannel = (filePath) => {
  if (!monFiles.includes(filePath)) {
    monFiles.push(filePath);
    fw.watchFile(filePath);
  }
};

const init = async () => {
  Log.info('Log monitor started', watchDir);
  try {
    await Fs.dirList(LOGS_DIR).then(list => {
      list.forEach(file => {
        initChannel(path.resolve(LOGS_DIR, file))
      });
    });

    await Fs.dirList(watchDir).then(list => {
      list.forEach(file => {
        initChannel(path.resolve(watchDir, file))
      });
    });

    fw.watchPath(LOGS_DIR);
    fw.watchPath(watchDir);

    fw.on('rename', (_path, filename) => {
      initChannel(path.resolve(_path, filename));
    });

    fw.on('remove', (_path, filename) => {
      initChannel(path.resolve(_path, filename));
    });

    fw.on('change', (_path, filename) => {
      initChannel(path.resolve(_path, filename));
    });

    fw.on('line', ({line, path}) => {
      const {stream, driver} = LogWacher.parsePath(path);
      const type = LogWacher.checkType(line, path);
      const cn = (stream || driver).toUpperCase();

      if (!channels.includes(cn)) {
        channels.push(cn);
      }

      // Log.warn('[line] -->', stream || driver, type, line);
      queue.addToQueue(Queue.EVENTS.LOG, {action: 'channels', channels});
      queue.addToQueue(Queue.EVENTS.LOG, {action: 'line', stream, driver, type, line});
    });

    queue.consume(Queue.EVENTS.LOG, (msg) => {
      $Faye.sendClient('log', msg);
    });
  } catch (e) {
    console.error(e);
  }
};

const checkPath = () => {
  watchDir = (new OpenServer()).logPath;
  if (fs.existsSync(watchDir) && !watchDir.includes($app.cwd)) {
    init();
  } else {
    setTimeout(checkPath, 300);
  }
};
checkPath();
