/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

import Vue from 'vue';
import AppPlugin from '~/core/app';

Vue.use(AppPlugin);
