/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://www.npmjs.com/package/js-cookie
 */

import Cookies from 'js-cookie';
import Time from './Time';

let setPrefix = (name = process.env.PANEL || '') => {
  return `__os_${name}_`;
};

class Cookie {
  constructor() {
    this.prefix = setPrefix();
  }

  get defaultOpts() {
    return {
      path: '/',
      domain: window.location.hostname,
      expires: this.getExpire(Time.Minute(30))
    };
  }

  getExpire(time = 0) {
    let d = new Date();
    d.setTime(d.getTime() + time);
    return d;
  }

  get(name, isJson = false) {
    return Cookies[isJson ? 'getJSON' : 'get'](this.prefix + name);
  }

  set(name, value, opts = {setPrefix: true, path: '/'}) {
    name = opts.setPrefix ? this.prefix + name : name;
    if (String(window.location.protocol).includes('https')) {
      opts.secure = true;
    }
    return Cookies.set(name, value, Object.assign(this.defaultOpts, opts));
  }

  remove(name, opts = {path: '/', setPrefix: true}) {
    this.set(name, '', Object.assign(opts, {expires: new Date('Fri Nov 20 1970 00:00:00')}));
  }
}

export default new Cookie();
