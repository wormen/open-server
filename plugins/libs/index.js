/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import Cookie from './cookie';
import Time from './Time';
import Translite from './translite';
import UUID from './Uuid';
import deepmerge from 'deepmerge';

export {
  Cookie,
  Time,
  Translite,
  UUID
}

/**
 * Генерируем хэш
 * @param length - длина хэша
 * @constructor
 */
export function GenerateHash(length = 20) {
  let n;
  let S = 'x';
  let hash = s => {
    if (typeof (s) === Number && s === parseInt(s, 10)) {
      s = Array(s + 1).join('x');
    }

    return s.replace(/x/g, () => {
      n = Math.round(Math.random() * 61) + 48;
      n = n > 57 ? (n + 7 > 90 ? n + 13 : n + 7) : n;
      return String.fromCharCode(n);
    });
  };

  for (let i = 0; i < length; i++) {
    S = S + 'x';
  }

  return hash(S);
}

export function plural(str, number) {
  str = String(str).split('|');
  let one = str[0];
  let two = str[1];
  let five = str[2];

  let n = Math.abs(number);
  n %= 100;
  if (n >= 5 && n <= 20) {
    return five;
  }
  n %= 10;
  if (n === 1) {
    return one;
  }
  if (n >= 2 && n <= 4) {
    return two;
  }
  return five;
}

/**
 * Генерируем query string на основе json
 * @param json
 * @returns {string}
 */
export function param(json) {
  if (!isObject(json) || Object.keys(json).length === 0) return '';

  return '?' +
    Object.keys(json).map(function (key) {
      return encodeURIComponent(key) + '=' +
        encodeURIComponent(json[key]);
    }).join('&');
}

/**
 * Пользовательское представление размера файлов
 * @param length
 * @returns {string}
 */
export function formatSize(length) {
  let i = 0
  let type = ['B', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb'];

  while ((length / 1000 | 0) && i < type.length - 1) {
    length /= 1024;
    i++;
  }
  return length.toFixed(2) + ' ' + type[i]
}

/**
 * Объединение объектов
 * @param origin
 * @param add
 * @param deep
 * @returns {*}
 */
function extend(origin, add, deep = false) {
  if (add === null || typeof add !== 'object') return origin;

  origin = deepmerge(origin, add);

  if (!deep) {
    return origin;
  } else {
    return JSON.parse(JSON.stringify(origin));
  }
}

export {extend}

export function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export function isNull(arg) {
  return arg === null;
}

export function isString(arg) {
  return typeof arg === 'string';
}

export function isUndefined(arg) {
  return arg === undefined;
}

export function isNullOrUndefined(arg) {
  return arg === null || arg === undefined;
}

function isObject(arg) {
  return arg !== null && typeof arg === 'object';
}

export {isObject}

export function isBoolean(arg) {
  return typeof arg === 'boolean';
}

export function isFunction(arg) {
  return typeof arg === 'function';
}
