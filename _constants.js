/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const path = require('path');
const App = require('./server/libs/App');

global.$app = new App();

global.ROOT_DIR = $app.cwd;
// global.MODULES_DIR = path.join(ROOT_DIR, 'modules');
// global.TOOLS_DIR = path.join(MODULES_DIR, 'tools');
global.RESOURES_DIR = path.join(ROOT_DIR, 'resources');
global.TOOLS_IN_DIR = path.join(RESOURES_DIR, 'tools'); // внешние приложения, котрые копируются в инсталяцию
global.SERVICES_DIR = path.join(ROOT_DIR, 'services'); // каталог пользоввательских сервисов для PM2
global.USERDATA_DIR = path.join(ROOT_DIR, 'userdata'); // корневой каталог для данных приложения
global.DATA_DIR = path.join(USERDATA_DIR, 'data'); // каталог для различных БД
global.APP_DATA_DIR = path.join(USERDATA_DIR, 'app-data'); // кэш електрона
global.SSH_KEYS_DIR = path.join(USERDATA_DIR, 'ssh-keys'); // каталог для хранения SSH ключей
global.DOWNLOADS_DIR = path.join(USERDATA_DIR, 'downloads');
global.LOGS_DIR = path.join(USERDATA_DIR, 'logs');
global.TMP_DIR = path.join(USERDATA_DIR, 'temp');
// global.SCRIPTS_DIR = path.join(ROOT_DIR, 'scripts');

// глобальные настройки приложения
global.APP_SETTING = Object.assign(require('./core/defaultData').APP_SETTING, {isReady: false});

process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;
process.env.ELECTRON_ENABLE_LOGGING = true;

global.GITLAB = {
  USER: 'wormen',
  TOKEN: 'Tb91RWdUQrKU1L3sJtB2',
  REPO: 'open-server',
  BRANCHE: 'master'
};

global.mergeEnv = (obj) => {
  process.env = Object.assign({}, process.env, obj);
};

mergeEnv({
  // PATH: [
  //   path.join(RESOURES_DIR, 'bin'),
  //   process.env.PATH
  // ].join(';'),
  ROOT_DIR,
  IS_DEV: process.env.NODE_ENV === 'DEV',
  OPEN_SERVER: {
    SYSTEM_NOTIFY: false, // использовать системные уведомления
    // ----
    EXEC: '', // название исполняемого файла
    ROOT_DIR: '',
    BACKUP_DIR: '',
    PROFILE: 'Default',
    VALID_ROOT_DIR: false,
    ACTIVE_MODULE: {},
    ACTIVE_DRIVER: {}
  }
});
mergeEnv({
  DEBUG_LEVEL: process.env.IS_DEV ? 'debug' : 'info',
  DOWNLOADS_DIR,
  TOOLS_IN_DIR,
  DATA_DIR,
  TMP_DIR
});

global.MQTT = {
  PORT: process.env.MQTT_PORT || 1883
};
