process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;

const pkg = require('./package');
const isDev = process.env.NODE_ENV === 'DEV';

let env = {
  HOST: process.env.HOST || '0.0.0.0',
  PORT: process.env.PORT || 11000
};

env.API_HOST = `http://localhost:${env.PORT}`;

module.exports = {
  env,
  mode: 'spa',
  router: { // https://nuxtjs.org/api/configuration-router
    linkActiveClass: 'active',
    linkExactActiveClass: 'exact-active',
    scrollBehavior: null,
    middleware: [
      'i18n'
    ],
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, `pages/404.vue`)
      });
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''}
    ],
    link: [
      {rel: 'shortcut icon', type: 'image/x-icon', href: '/icons/16x16.png'}
    ]
  },
  build: {
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        // Run ESLint on save
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      // Extend only webpack config for client-bundle
      if (isClient) {
        config.target = 'electron-renderer'
      }
    }
  },
  dev: isDev,
  css: [
    '~/assets/css/main.css',
    // '~/assets/css/msp-style.css',
    '~/assets/css/msp-general.css',
    '~/assets/css/msp-components.css',
    '~assets/fonts/font-mfizz/font-mfizz.css', // http://fizzed.com/oss/font-mfizz
    'animate.css/animate.min.css'
  ],
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/font-awesome'
  ],
  plugins: [
    '~/plugins/api',
    {src: '~/plugins/app', ssr: false},
    '~/plugins/i18n.js',
    {src: '~/plugins/faye', ssr: false},
    {src: '~/plugins/notify', ssr: false}
  ],
  loading: false
  // : {
  // color: '#C21A26'
  // }
};
