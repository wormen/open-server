/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://electronjs.org/docs/api/ipc-renderer
 */

import {ipcRenderer} from 'electron';
import {isJson} from '~/plugins/libs';

const noop = () => {
};

export default class IPC {
  _encode(val) {
  }

  _decode(val) {
    if (Array.isArray(val) || isJson(val)) {
      try {
        val = JSON.parse(val);
      } catch (e) {
      }
    }
    return val;
  }

  on(channel, listener = noop) {
    ipcRenderer.on(channel, (event, val) => {
      listener(event, this._decode(val));
    });
  }

  once(channel, listener = noop) {
    ipcRenderer.once(channel, (event, val) => {
      listener(event, this._decode(val));
    });
  }

  remove(channel, listener = noop) {
    ipcRenderer.removeListener(channel, listener);
  }

  removeAll(channel) {
    ipcRenderer.removeAllListeners(channel);
  }

  send(channel, ...args) {
    ipcRenderer.send(channel, ...args);
  }

  sendSync(channel, ...args) {
    ipcRenderer.sendSync(channel, ...args);
  }

  sendTo(windowId, channel, ...args) {
    ipcRenderer.sendTo(windowId, channel, ...args);
  }
}
