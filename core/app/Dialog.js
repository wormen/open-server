/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {dialog} = require('electron').remote;

class Dialog {
  selectDirectory(done) {
    dialog.showOpenDialog({properties: ['openDirectory']}, (filePaths) => {
      done(filePaths ? filePaths[0] : null);
    });
  }

  selectFile(filters = [{name: 'Все файлы', extensions: ['*']}], done) {
    dialog.showOpenDialog({
      properties: ['openFile'],
      filters
    }, (filePaths) => {
      filePaths = filePaths ? filePaths[0] : null;
      let separator = '/';
      if (filePaths) {
        if (filePaths.includes('\\')) {
          separator = '\\';
        }
        filePaths = String(filePaths).split(separator).pop();
      }
      done(filePaths);
    });
  }

  selectPath(filters = [{name: 'Все файлы', extensions: ['*']}], done) {
    dialog.showOpenDialog({
      properties: ['openFile'],
      filters
    }, (filePaths) => {
      filePaths = filePaths ? filePaths[0] : '';
      done(filePaths);
    });
  }
}

export default Dialog;
