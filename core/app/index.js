/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import App from './App';
import Ipc from './Ipc';
import Dialog from './Dialog';

export default {
  install(Vue, options) {
    if (!Vue.prototype.hasOwnProperty('$app')) {
      Vue.prototype.$app = new App();
      Vue.prototype.$ipc = new Ipc();
      Vue.prototype.$dialog = new Dialog();
    }
  }
}
