/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {remote} = require('electron');

export default class App {
  get env() {
    return remote.process.env;
  }

  get cwd() {
    return this.env.ROOT_DIR;
  }
}
