/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import axios from '~/plugins/axios';
import Api from './Api';

export default class Ssh extends Api {
  constructor() {
    super();
    this.url = super.fullUrl(`/ssh`);
  }

  getConnections() {
    return axios.get(`${this.url}/getConnections`, super.defaultOpts)
      .then(super.response);
  }

  getItem(id) {
    return axios.get(`${this.url}/getItem/${id}`, super.defaultOpts)
      .then(super.response);
  }

  openTerminal(id) {
    return axios.get(`${this.url}/openTerminal/${id}`, super.defaultOpts)
      .then(super.response);
  }

  openPuttyTerminal(id) {
    return axios.get(`${this.url}/openPuttyTerminal/${id}`, super.defaultOpts)
      .then(super.response);
  }

  terminalCommand(id, cmd) {
    return axios.put(`${this.url}/terminalCommand/${id}`, {cmd}, super.defaultOpts)
      .then(super.response);
  }

  closeTerminal(id) {
    return axios.delete(`${this.url}/closeTerminal/${id}`, super.defaultOpts)
      .then(super.response);
  }

  save(data) {
    return axios.post(`${this.url}/save`, data, super.defaultOpts)
      .then(super.response);
  }

  update(id, data) {
    delete data.id;
    return axios.put(`${this.url}/update/${id}`, data, super.defaultOpts)
      .then(super.response);
  }

  remove(id) {
    return axios.delete(`${this.url}/remove/${id}`, super.defaultOpts)
      .then(super.response);
  }

  getKeys() {
    return axios.get(`${this.url}/getKeys`, super.defaultOpts)
      .then(super.response);
  }

  getKey(name) {
    return axios.get(`${this.url}/getKey/${name}`, super.defaultOpts)
      .then(super.response);
  }

  saveKey(data) {
    return axios.post(`${this.url}/saveKey`, data, super.defaultOpts)
      .then(super.response);
  }

  removeKey(name) {
    return axios.delete(`${this.url}/removeKey/${name}`, super.defaultOpts)
      .then(super.response);
  }
}
