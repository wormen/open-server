/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import axios from '~/plugins/axios';
import Api from './Api';

export default class Mysql extends Api {
  constructor() {
    super();
    this.url = super.fullUrl(`/mysql`);
  }

  getList() {
    return axios.get(`${this.url}/getList`, super.defaultOpts)
      .then(super.response);
  }
}
