/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

module.exports.APP_SETTING = {
  app: {
    nativeNotify: false,
    closeInTray: false
  },
  OS: {
    dir: '', // расположение OS
    www: '', // каталог расположения доменов
    file: '' // использовать исполняемый файл
  },
  sys: {
    liveTime: 5,
    backupDir: '' // каталог для бэкапов
  },
  general: { // userdata\init.ini
    autostart: true, // автозапуск OS
    wait: 20, // задержка перед запуском в сек
    clearlogs: true,
    checkupdate: true,
    checkupdatePanel: true,
    whithWin: false
  },
  server: {
    dns: {
      refresh: 30
    },
    ftp: {
      isStartServer: true,
      connecttimeout: 60,
      commandtimeout: 600
    },
    proxy: {
      autoOn: false,
      logOn: true,
      port: 9100
    },
    mysql: {
      rootPassword: ''
    },
    postgres: {
      rootPassword: ''
    },
    charset: {
      http: 'notset',
      mysql: 'utf8_general_ci'
    },
    ports: {
      HTTP: 80,
      HTTPS: 443,
      FTP: 21,
      FTPS: 990,
      PHP: 9000,
      Backend: 8080,
      MySQL: 3306,
      Redis: 6309,
      MongoDB: 27017,
      Postgres: 5432,
      Memcache: 11211
      // ,
      // Proxy: 9005
    },
    ipArr: [
      {value: '*', title: 'Все доступные IP', isActive: true}
    ],
    debugmode: false,
    astart: false, // агрессивный старт
    selfhosts: false, // Не вносить изменения в HOSTS
    allow: false, // Защитить сервер от внешнего доступа

    crpath: 0, // настройка PATH
    pathArr: []
  },
  sysSoft: {
    logs: '', // открывать логи в программе ...
    browser: '' // открывать страницы в ...
  }
};
