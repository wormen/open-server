/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const Faye = require('faye');
const noop = () => {
};

export default class FayeClient {
  constructor() {
    this.client = null;
    this.isOnline = false;
  }

  connectInit() {
    this.connect();
  }

  connect() {
    let url = process.env.API_HOST;
    this.client = new Faye.Client(`${url}/client`, {
      timeout: 60
    });

    this.client.on('transport:down', () => {
      this.isOnline = false;
    });

    this.client.on('transport:up', () => {
      this.isOnline = true;
    });
  }

  setChannel(channel) {
    return '/' + ['client', channel].join('/');
  }

  publish(channel, data) {
    this.client.publish(this.setChannel(channel), data);
  }

  subscribe(channel, cb = noop) {
    if (String(channel).includes('*')) {
      return this.client.subscribe(this.setChannel(channel)).withChannel((cn, message) => {
        cb(cn, message);
      });
    }

    this.client.subscribe(this.setChannel(channel), cb);
  }

  unsubscribe(channel, cb = noop) {
    this.client.unsubscribe(this.setChannel(channel), cb);
  }
}
