/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://github.com/imagemin/imagemin
 */

const fs = require('fs');
const path = require('path');
const pngToIco = require('png-to-ico');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');

const src = 'static/icons-src';
const dest = 'static/icons';

(async () => {
  await imagemin([src + '/*.{jpg,png}'], dest, {
    plugins: [
      imageminJpegtran(),
      imageminPngquant({quality: '65-80'})
    ]
  })
    .then(files => {
      console.log(files);
      //=> [{data: <Buffer 89 50 4e …>, path: 'build/images/foo.jpg'}, …]
      return files;
    });

  const fileList = fs.readdirSync(src)
    .filter(file => {
      return !file.includes('512') && file.includes('.png');
    })
    .map(file => {
      return path.join(dest, file);
    });

  let filesArr = [];
  for (let item of fileList) {
    filesArr.push(fs.readFileSync(item));
  }

  pngToIco(path.join(dest, '512x512.png')).then(buf => {
    fs.writeFileSync(path.join(dest, 'favicon.ico'), buf);
  });

})();
