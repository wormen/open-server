/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';
import {APP_SETTING} from '../core/defaultData';

const state = {
  locales: ['ru', 'en'],
  locale: 'ru',
  localeListFull: {
    ru: 'Русский',
    en: 'English'
  },
  isLaunch: false,
  requireRestartOS: false,
  ENV: {
    VALID_ROOT_DIR: true,
    ACTIVE_MODULE: {}
  },
  on: { // список активных модулей
    terminalWidget: false
  },
  settings: APP_SETTING,
  logChannels: [],
  logState: {}
};

export default state;
