/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';
import {APP_SETTING} from '../core/defaultData';
import {extend} from '~/plugins/libs';

let Cookie = process.browser ? require('~/plugins/libs').Cookie : null;

const mutations = {
  SET_LANG(state, locale) {
    let dir = [];

    if (state.locales.includes(locale)) {
      state.locale = locale;
      state.dir = dir.includes(locale) ? 'rtl' : 'ltr';
      if (process.browser) {
        Cookie.set('locale', locale);
      }
    }
  },
  SET_LAUNCH(state, val) {
    if (state.isLaunch === val) {
      return;
    }
    state.requireRestartOS = false;
    state.isLaunch = val;
  },
  SET_ENV(state, obj) {
    state.ENV = obj;
  },
  RESTART_OS(state, val) {
    state.requireRestartOS = false;
    if (state.isLaunch && val) {
      state.requireRestartOS = val;
    }
  },
  SET_SETTING(state, val) {
    state.settings = extend(APP_SETTING, val, true);
  },
  SET_LOG_CHANNELS(state, list) {
    state.logChannels = list;
  },
  LOG_PUSH(state, n) {
    let st = (n.stream || n.driver).toUpperCase();
    if (!state.logState.hasOwnProperty(st)) {
      state.logState[st] = [];
    }
    if (state.logState[st].length > 500) {
      state.logState[st].splice(0, 1);
    }
    state.logState[st].push(n);
  }
};

export default mutations;
