require('./_constants');

const pkg = require('./package');
const fs = require('fs');
const path = require('path');
const http = require('http');
const {Nuxt, Builder} = require('nuxt');
const request = require('request');
const electron = require('electron');
const Fs = require('./server/libs/Fs');
const Log = require('./server/libs/Log');
// const PM2 = require('./server/pm2');
const Mqtt = require('./server/mqtt');
const Launcher = require('./server/launcher');
const App = require('./server/libs/App');
const IPC = require('./server/ipc');
const {extend, isObject} = require('./server/utils');

// const pm2 = new PM2();
let config = require('./nuxt.config.js');
config.rootDir = __dirname; // for electron-builder

/**
 * Переопределяем пути системных каталогов
 * https://electronjs.org/docs/all
 */
electron.app.setPath('userData', APP_DATA_DIR);
electron.app.setPath('appData', USERDATA_DIR);
electron.app.setPath('downloads', DOWNLOADS_DIR);
electron.app.setPath('logs', LOGS_DIR);
electron.app.setPath('temp', TMP_DIR);

// Init Nuxt.js
const nuxt = new Nuxt(config);
const builder = new Builder(nuxt);
const guiServer = http.createServer(nuxt.render);

// Build only in dev mode
if (config.dev) {
  builder.build().catch(err => {
    Log.error(err);
    process.exit(1)
  })
}

let isRunning = false; // флаг, указывающий, что главное окно открыто
let isRunningTime = 0;

(async () => {
  try {
    const createDirs = () => {
      return new Promise(async (resolve) => {
        const dirs = [
          'SERVICES_DIR',
          'LOGS_DIR', 'DATA_DIR', 'TMP_DIR', 'DOWNLOADS_DIR',
          'APP_DATA_DIR'
        ];

        for (let dir of dirs) {
          await Fs.emptyDir(global[dir])
        }

        for (let dir of dirs) {
          if (!fs.existsSync(global[dir])) {
            console.log(`${global[dir]} not found`);
            // return createDirs();
          } else {
            console.log(`${global[dir]} exists`);
          }
        }

        resolve();
      });
    };

    Log.info('Creating app directories ....');
    await createDirs();
    Log.info('Directories created');

    const Server = require('./server');

    // Сервер очередей
    let mp = process.env.MQTT_PORT || 1883;
    (new Mqtt.Server()).listen(mp).on('ready', (port) => {
      Log.info('MQTT Server listening on localhost:' + port);
    });

    // Listen the API server
    await Server.listen(config.env.PORT, config.env.HOST);

    // Listen the UI server
    guiServer.listen();
    const _NUXT_URL_ = `http://localhost:${guiServer.address().port}`;
    Log.info(`Nuxt GUI working on ${_NUXT_URL_}`);

    /*
    ** Electron
    */
    let win = null; // Current window
    const app = electron.app;
    const tray = App.Tray;

    const newWin = (goUrl) => {
      goUrl = typeof goUrl === 'string' ? goUrl : _NUXT_URL_;

      tray.init();

      win = new electron.BrowserWindow({
        title: pkg.name,
        show: false,
        icon: $app.icon,
        width: 1220,
        height: 740,
        webPreferences: {
          webviewTag: false,
          nodeIntegrationInWorker: true
        }
      });

      // win.maximize()
      win.on('closed', () => {
        win = null
      });

      if (config.dev) {
        // Install vue dev tool and open chrome dev tools
        const {default: installExtension, VUEJS_DEVTOOLS} = require('electron-devtools-installer');
        installExtension(VUEJS_DEVTOOLS.id).then(name => {
          console.log(`Added Extension:  ${name}`);
        }).catch(err => console.log('An error occurred: ', err));

        const pollServer = () => {
          http.get(goUrl, (res) => {
            if (res.statusCode === 200) {
              win.loadURL(goUrl);
              win.show();
              tray.emit('setShow', true);
              win.webContents.openDevTools();
              isRunning = true;
            } else {
              setTimeout(pollServer, 300)
            }
          }).on('error', pollServer)
        };
        pollServer();
      } else {
        win.loadURL(goUrl);
        win.show();

        tray.emit('setShow', true);
        isRunning = true;
        // win.webContents.openDevTools()
      }
    };

    app.on('ready', () => {
      newWin();
      tray.on('show', newWin)
        .on('go', pathName => {
          $Faye.sendClient('go', pathName);
        });
    });
    app.on('activate', () => win === null && newWin());
    app.on('window-all-closed', () => {
      try {
        let isMin = APP_SETTING.app.closeInTray || false;
        if (APP_SETTING.isReady && isMin) {
          if (win) {
            win.hide();
          }
          tray.emit('setShow', false);
        } else {
          app.quit()
        }
      } catch (e) {
        app.quit();
      }
    });

    // pm2.connect();
    // pm2.on('ready', () => {
    //   Log.info('pm2 ready', new Date());
    //   pm2.services();
    // });
    Launcher.startServices({
      proxy: {
        ignore: [
          ['localhost', config.env.PORT].join(':'),
          [config.env.HOST, config.env.PORT].join(':'),
          `localhost:${guiServer.address().port}`
        ]
      }
    });
    isRunningTime = Date.now();
    checkRelaunch();

    await (require('./server/libs/os')).init();

    IPC.on('save-setting', (event, val) => {
      if (isObject(val)) {
        APP_SETTING = extend(APP_SETTING, val.settings, true);
        APP_SETTING.isReady = true;
      }
    });

    IPC.on('app::show-main', () => win === null && newWin());

    IPC.on('app::init', async () => {
      $app.init();
      Server.monitoring();
    });

    IPC.on('app::exit', () => {
      app.quit();
    });

    IPC.on('check-update', (event, val) => {
      request(`https://gitlab.com/${GITLAB.USER}/${GITLAB.REPO}/raw/master/package.json?ref=${GITLAB.BRANCHE}`, {
          headers: {
            'PRIVATE-TOKEN': GITLAB.TOKEN
          },
          method: 'GET'
        },
        (err, res, body) => {
          if (!err) {
            event.sender.send('check-update', body);
          }
        });
    });
  } catch (e) {
    Log.error(e);
    // выводим окно с ошибкой
    electron.dialog.showErrorBox('Ошибка при инициализации', e);
  }
})();

// проверяем, нужен ли перезапуск приложения
function checkRelaunch() {
  let diff = Date.now() - isRunningTime;
  let minutes = 3e4; // 30 сек
  if (isRunning === false && diff >= minutes) {
    electron.app.relaunch({args: process.argv.slice(1).concat(['--relaunch'])});
    electron.app.exit(0)
  }
  setTimeout(checkRelaunch, 500);
}
