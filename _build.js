/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://github.com/electron-userland/electron-builder/blob/master/test/src/windows/portableTest.ts
 */

'use strict';

const pkg = require('./package');
const path = require('path');
const {spawn} = require('child_process');

const Fs = require('./server/libs/Fs');
const builder = require('electron-builder');
const Platform = builder.Platform;
const Arch = builder.Arch;

// "build": "nuxt build && electron-builder",
//   "prebuild": "electron-rebuild -f -w diskusage,sqlite3,addon-process,addon-drives"

const BIN = path.resolve(__dirname, 'node_modules', '.bin');
const NUXT = path.resolve(BIN, 'nuxt.cmd');
const ELECTRON_REBUILD = path.resolve(BIN, 'electron-rebuild.cmd');

(async () => {
  if (pkg.build.win.target === 'dir') {
    await build(false);
  } else {
    for (let arch of ['ia32', 'x64']) {
      console.log('');
      console.log('Build arch --->', arch);
      await build(true, arch)
    }
  }
})();

async function build(isProd, arch) {
  let args = [
    '-f',
    '-w', 'diskusage,sqlite3,addon-process,addon-drives'
  ];
  let opts = {
    targets: Platform.WINDOWS.createTarget('zip', Arch[arch]),
    config: pkg.build
  };
  if (isProd) {
    args.push('--arch', arch);
  }

  arch = (arch === 'x64' ? arch : 'x86');
  opts.config.artifactName = "${productName}_" + arch + "__${version}.${ext}"; // eslint-disable-line

  await runCmd(ELECTRON_REBUILD, args);
  await runCmd(NUXT, ['build']);
  await builder.build(opts);

  if (isProd) {
    await Fs.move(
      path.resolve(__dirname, 'dist', `${opts.config.productName}_${arch}__${pkg.version}.zip`),
      path.resolve(__dirname, 'release', `${opts.config.productName}_${arch}__${pkg.version}.zip`)
    )
  }
}

function runCmd(cmd, args = []) {
  return new Promise((resolve, reject) => {
    let app = spawn(cmd, args, {
      cwd: __dirname,
      env: process.env
    });

    app.stdout.on('data', (data) => {
      console.log(data.toString());
    });
    app.stderr.on('data', (data) => {
      console.error(data.toString());
    });
    app.on('close', code => {
      if (code === 0) {
        resolve(code);
      } else {
        reject(code);
      }
    });
  });
}
