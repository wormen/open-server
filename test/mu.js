/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const l = [
  "dev@002dcrawler"
  // ,
  // "dev@002deleanor@002d1",
  // "dev@002deleanor@002dn",
  // "dev@002deleanor@002dn@0020@0028backup@0029",
  // "dev@002dkitmall",
  // "dev@002dtrash",
  // "ninja@002ddog@0028B@0029",
  // "pro@002davto@002dclub",
  // "spb@002doptom"
].map(item => {
  item = String(item);
  if (item.includes('@')) {
    for (let u of item.match(/\@[a-zA-Z0-9]{4}/g)) {
      item = item.replace(u, JSON.parse(`"\\u${u.replace(/\@/, '')}"`));
    }
    console.log(item)
  }
  return item;
});


function utf8Decode(strUtf) {
  // note: decode 3-byte chars first as decoded 2-byte strings could appear to be 3-byte char!
  // note: decode 3-byte chars first as decoded 2-byte strings could appear to be 3-byte char!
  return String(strUtf).replace(
    /[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g,  // 3-byte chars
    function (c) {  // (note parentheses for precedence)
      var cc = ((c.charCodeAt(0) & 0x0f) << 12) | ((c.charCodeAt(1) & 0x3f) << 6) | (c.charCodeAt(2) & 0x3f);
      return String.fromCharCode(cc);
    }
  ).replace(
    /[\u00c0-\u00df][\u0080-\u00bf]/g,                 // 2-byte chars
    function (c) {  // (note parentheses for precedence)
      var cc = (c.charCodeAt(0) & 0x1f) << 6 | c.charCodeAt(1) & 0x3f;
      return String.fromCharCode(cc);
    }
  );
}
