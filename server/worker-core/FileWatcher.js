/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const fs = require('fs');
const $path = require('path');
const {EventEmitter} = require('events');
const Log = require('../libs/Log');

class FileWatcher extends EventEmitter {
  /**
   * Обработка событий
   * @param event
   * @param path
   * @param filename
   */
  eventHandle(event, path, filename) {
    // Log.debug(`[WATCHER]`, event, path, filename);
    this.emit(event, path, filename);
  }

  /**
   * Наблюдаем за изменениями ресурса
   * @param path
   * @returns {*}
   */
  watchPath(path) {
    let watcher;

    if (!fs.existsSync(path)) {
      return setTimeout(() => this.watchPath(path), 200);
    }

    let opts = {
      persistent: true,
      recursive: true
    };

    watcher = fs.watch(path, opts, (event, filename) => {
      if (event === 'rename') {
        if (!fs.existsSync($path.resolve(path, filename))) {
          event = 'remove';
        }
        this.eventHandle(event, path, filename);

        watcher.close();
        this.watchPath(path);
      }

      if (event === 'change') {
        if (!fs.existsSync($path.resolve(path, filename))) {
          event = 'remove';
          this.eventHandle(event, path, filename);
        }
        this.eventHandle(event, path, filename);
      }
    });
  }

  watchFile(path) {
    let currSize = 0;

    if (!fs.existsSync(path)) {
      currSize = 0;
      setTimeout(() => this.watchFile(path), 200);
      return;
    }

    currSize = fs.statSync(path).size;
    let watcher = fs.watch(path, (event, filename) => {
      if (event === 'rename') {
        watcher.close();
        this.watchFile(path);
      }
      if (event === 'change') {
        if (!fs.existsSync(path)) {
          currSize = 0;
          setTimeout(() => this.watchFile(path), 200);
          return;
        }
        fs.stat(path, (err, stat) => {
          try {
            this.readNewLogs(path, stat.size, currSize);
            currSize = stat.size;
          } catch (e) {
            Log.error(e);
          }
        });
      }
    });
  }

  /**
   * Зачитываем измеенния файла
   * @param path
   * @param curr
   * @param prev
   * @returns {module:fs.ReadStream}
   */
  readNewLogs(path, curr, prev) {
    if (curr < prev) {
      return;
    }
    let rstream = fs.createReadStream(path, {
      encoding: 'utf8',
      start: prev,
      end: curr
    });

    return rstream.on('data', (data) => {
      let line;
      let lines = data.split(`\n`);
      let _len;
      let _results = [];

      for (let _i = 0, _len = lines.length; _i < _len; _i++) {
        line = lines[_i];
        if (line) {
          _results.push(this.emit('line', {line: line.trim(), path}));
        }
      }
      return _results;
    });
  }
}

module.exports = FileWatcher;
