/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

class WorkerCore {
}

module.exports = WorkerCore;
module.exports.FileWatcher = require('./FileWatcher');
module.exports.LogWacher = require('./LogWacher');
