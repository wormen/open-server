/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const path = require('path');

class LogWacher {
  static parsePath(dir, file = null) {
    if (arguments.length === 1) {
      file = dir;
    }
    const fullPath = arguments.length === 1 ? dir : path.resolve(dir, file);
    let stream = checkStream(file).toUpperCase();
    let driver = checkDriver(file).toUpperCase();
    let obj = {
      stream,
      driver,
      fullPath
    };

    return obj;
  }

  /**
   * https://log4js-node.github.io/log4js-node/layouts.html
   * @param line
   * @param _path
   * @returns {string}
   */
  static checkType(line, _path) {
    let type = 'out';
    for (let _type of ['info', 'notice', 'error', 'debug', 'trace', 'warn', 'fatal']) {
      if (String(line).toLowerCase().includes(_type) || String(_path).toLowerCase().includes(_type)) {
        type = _type;
        break;
      }
    }
    return type;
  }
}

module.exports = LogWacher;

function checkDriver(file) {
  let driver = '';
  Object.entries(process.env.OPEN_SERVER.ACTIVE_DRIVER).forEach(([drv, v]) => {
    if (String(file).includes(v)) {
      driver = drv;
    }
  });
  if (String(file).includes('FileZilla')) {
    driver = 'ftp';
  }
  if (String(file).includes('Bind')) {
    driver = 'dns';
  }
  return driver;
}

function checkStream(file) {
  let stream = '';
  if (String(file).includes('app-panel')) {
    stream = 'os panel';
  }
  if (String(file).includes('proxy')) {
    stream = 'proxy';
  }
  if (String(file).includes('main')) {
    stream = 'main';
  }
  if (String(file).includes('mail')) {
    stream = 'email';
  }
  return stream;
}
