/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const pkg = require('../package');
const http = require('http');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const Faye = require('./libs/Faye');
const Monitoring = require('./monitoring');
const Process = require('./libs/Process');
const Log = require('./libs/Log');

const api = require('./api/v1');
global.$Faye = new Faye();

let headers = [
  'Content-Type', 'Accept', 'Cookie',
  'Origin', 'X-Requested-With', 'User-Agent',
  'Keep-Alive', 'Authorization', 'X-Locale',
  'X-Compress', 'X-Panel', 'X-SDK', 'X-Service'
];

let headersArr = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,OPTIONS',
  'Access-Control-Allow-Headers': headers.join(','),
  'Access-Control-Allow-Credentials': 'true',
  'X-XSS-Protection': ' 1; mode=block',
  'X-Content-Type-Options': ' nosniff',
  'X-Powered-By': pkg.name
};

class ApiServer {
  static CORS(req, res, next) {
    for (let k in headersArr) {
      res.setHeader(k, headersArr[k]);
    }

    if (typeof next === 'function') {
      next();
    }
  }

  static listen(port, host) {
    const app = express();

    app
      .set('port', port)
      .use(bodyParser.json())
      .use(bodyParser.urlencoded({extended: false}))
      .use(cookieParser());

    app.use(this.CORS);

    // Import API Routes
    app.use('/api/v1', api);

    return new Promise((resolve) => {
      const server = http.createServer(app).listen(port, host, () => {
        Log.info('HTTP Server listening on http://' + host + ':' + port);
        resolve(server);
      });

      // Faye сервер
      $Faye.attach(server);
    });
  }

  static sendEnv() {
    setTimeout(() => {
      $Faye.sendClient(`system/env`, process.env.OPEN_SERVER);
      this.sendEnv();
    }, 1e3);
  }

  static monitoring() {
    this.sendEnv();

    const mn = new Monitoring();
    const _Process = new Process();

    mn.on('cpu', data => {
      $Faye.sendClient(`system/cpu`, data);
    });
    mn.on('disk', data => {
      $Faye.sendClient(`system/drives`, data);
    });
    mn.on('memory', data => {
      $Faye.sendClient(`system/memory`, data);
    });
    Monitoring.start();

    _Process.setAutoPID(process.env.OPEN_SERVER.PID || 0);
    _Process.autoList(true);
    _Process.on('autoList:data', list => {
      let obj = {
        isLaunch: list.length > 1 && list[0].hasOwnProperty('pid'),
        list
      };
      _Process.setAutoPID(process.env.OPEN_SERVER.PID);
      $Faye.sendClient(`system/processes`, obj);
    });
    Process.start();
  }
}

module.exports = ApiServer;
