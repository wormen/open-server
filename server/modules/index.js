/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

module.exports = {};

for (let module of [
  'Apache', 'Nginx',
  'MySQL', 'Redis', 'Mongo',
  'Ssh'
]) {
  module.exports[module] = require(`./${module}`);
}
