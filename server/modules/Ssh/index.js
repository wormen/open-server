/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://www.npmjs.com/package/node-ssh
 https://www.npmjs.com/package/ssh-exec
 https://www.npmjs.com/package/to-ssh
 https://www.npmjs.com/package/simple-ssh
 https://www.npmjs.com/package/ssh-pool
 https://github.com/mscdex/ssh2

 https://github.com/sperka/node-sshclient

 https://github.com/cmp-202/ssh2shell

 https://stackoverflow.com/questions/5180981/ssh-client-for-node-js
 https://hub.packtpub.com/making-simple-web-based-ssh-client-using-nodejs-and-socketio/
 */

class SshModule {

}

module.exports = new SshModule();
module.exports.Gen = require('./Keygen');
