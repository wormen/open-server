/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Отправка уведомлений в UI
 */

const Queue = require('../mqtt');
const queue = new Queue();

class Notify {
  static send(type, message, opts = {}) {
    queue.addToQueue(Queue.EVENTS.NOTIFY, {type, message, opts});
  }
}

module.exports = Notify;
