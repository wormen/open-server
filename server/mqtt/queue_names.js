/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

module.exports = {
  BACKUP: 'backup',
  RESTORE_BACKUP: 'restore_backup',
  LOG: 'log',
  LOG_CHANNELS: 'log_channels',
  DEBUG: 'debug_queue',
  NOTIFY: 'notify_queue',
  PROXY: 'proxy_log',
  SAVE_SETTING: 'setting_data' // данные приходят, когда настройки сохраняются
};
