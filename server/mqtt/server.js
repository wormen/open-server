/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const path = require('path');
const {EventEmitter} = require('events');
const mosca = require('mosca');
const Fs = require('../libs/Fs');

const noop = () => {
};

class MqttServer extends EventEmitter {
  constructor() {
    super();
    this.store = path.resolve(USERDATA_DIR, 'mqtt');
    Fs.emptyDir(this.store);
  }

  listen(port = 1883, cb = noop) {

    const server = new mosca.Server({
      type: 'mqtt',
      allowNonSecure: true,
      host: '127.0.0.1',
      port
    });

    const db = new mosca.persistence.Memory();
    // const db = new mosca.persistence.LevelUp({
    //   path: this.store
    // });
    db.wire(server);

    server.on('ready', () => {
      super.emit('ready', port);
      cb(port);
    });

    return this;
  }
}

module.exports = MqttServer;
