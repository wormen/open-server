/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');

class Terminal extends EventEmitter {
  constructor(id) {
    super();

    this._id = id;
    this._cn = `/terminal/${id}`;

    this._init(id);

    super.on('log', line => {
      $Faye.sendClient(this._cn, {cmd: 'log', line});
    });
  }

  async _init(id) {
  }

  open() {
    return new Promise((resolve, reject) => {
    });
  }

  sendCommand(cmd) {
  }

  close() {
    return new Promise((resolve, reject) => {
      $Faye.sendClient(this._cn, {cmd: 'close'});
      resolve();
    });
  }
}

module.exports = Terminal;
