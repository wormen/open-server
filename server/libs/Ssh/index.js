/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const path = require('path');
const {spawn} = require('child_process');
const {SshConnettions} = require('../../db');
const Terminal = require('./Terminal');
const Fs = require('../Fs');
const {isJson} = require('../../utils');

let terminals = {};

class Ssh {
  openTerminal(id) {
    return new Promise((resolve, reject) => {
      terminals[`t${id}`] = new Terminal(id);
      terminals[`t${id}`].open()
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Запусе Putty терминала
   * https://www.eisscholle.net/spickzettel/windows/putty_parameter
   * @param id - идентификатор настроек в БД
   * @returns {Promise<Model.dataValues>}
   */
  openPuttyTerminal(id) {
    return SshConnettions.findById(id).then(({dataValues}) => {
      const putty = path.resolve(TOOLS_IN_DIR, 'putty', 'putty.exe');
      const plink = path.resolve(TOOLS_IN_DIR, 'putty', 'plink.exe');
      const {name, login, host, port, password, keyType, keyName} = dataValues;
      let args;
      try {
        if (dataValues.useKey) {
          args = ['-i', this._keyPath(keyName, keyType || 'putty')];
        } else {
          args = ['-pw', password];
        }
        // -m <filename> // удаленный запуск скрипта
        spawn(putty, [].concat(['-ssh', '-C', `${login}@${host}`, '-P', port], args), {
          detached: true,
          windowsVerbatimArguments: true,
          env: process.env
        });
        // if (dataValues.useKey) {
        //   args = ['-i', this._keyPath(keyName)];
        // } else {
        //   args = ['-pw', password];
        // }
        // spawn(path.resolve(TOOLS_IN_DIR, 'putty', 'plink.exe'), [].concat([
        //   '-ssh', '-C', '-noagent', `${login}@${host}`, '-P', port
        // ], args), {
        //   detached: true,
        //   windowsVerbatimArguments: true,
        //   env: process.env
        // });
      } catch (e) {
        console.error(e);
      }

      return dataValues;
    });
  }

  terminalCommand(id, cmd) {
    return new Promise((resolve, reject) => {
      if (terminals.hasOwnProperty(`t${id}`)) {
        terminals[`t${id}`].sendCommand(cmd);
        resolve();
      } else {
        reject(new Error('Terminal not found'));
      }
    });
  }

  closeTerminal(id) {
    return new Promise((resolve, reject) => {
      if (terminals.hasOwnProperty(`t${id}`)) {
        terminals[`t${id}`].close()
          .then(() => {
            resolve();
            delete terminals[`t${id}`];
          })
          .catch(reject);
      }
      resolve();
    });
  }

  getConnections() {
    return SshConnettions.findAll({
      attributes: ['id', 'name']
    });
  }

  save(data) {
    data.data = JSON.stringify(data);
    return SshConnettions.create(data);
  }

  update(id, data) {
    return SshConnettions.update(data, {where: {id}});
  }

  remove(id) {
    return SshConnettions.destroy({
      where: {id}
    });
  }

  _keyPath(name, type = 'putty') {
    let ext = 'pub';
    switch (type) {
      case 'putty':
        ext = 'ppk';
        break;
    }
    return path.resolve(SSH_KEYS_DIR, `${name}.${ext}`);
  }

  getKeys() {
    return Fs.dirList(SSH_KEYS_DIR).then(list => {
      return list.filter(item => {
        return String(item).includes('.pub');
      }).map(file => {
        let name = String(file).split('.pub')[0];
        if (Array.isArray(name)) {
          name = name.join('');
        }
        return {name}
      });
    });
  }

  getKey(name) {
    return Fs.readFile(this._keyPath(name));
  }

  saveKey(name, content) {
    return Fs.writeFile(this._keyPath(name), content);
  }

  removeKey(name) {
    return Fs.remove(this._keyPath(name));
  }
}

module.exports = Ssh;
