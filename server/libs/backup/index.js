/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const Pack = require('./files/Pack');
const Unpack = require('./files/Unpack');
const nameItem = require('./item');
const GenerateList = require('./GenerateList');

const Files = {
  Pack,
  Unpack
};

module.exports = {};
module.exports.Files = Files;
module.exports.nameItem = nameItem;
module.exports.GenerateList = GenerateList;
