/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const Backup = require('../main');
const targz = require('targz');

class Unpack extends Backup {
  constructor(src, dest) {
    super();

    this._src = src;
    this._dest = dest;

    this._init();
  }

  _init() {
    targz.decompress({
      src: this._src,
      dest: this._dest
    }, (err) => {
      if (err) {
        return this.emit('error', err);
      }
      this.emit('done');
    });
  }
}

module.exports = Unpack;
