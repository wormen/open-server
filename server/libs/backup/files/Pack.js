/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const Backup = require('../main');
const targz = require('targz');

class Pack extends Backup {
  constructor(src, dest, ignore = []) {
    super();

    this._src = src;
    this._dest = dest;
    this._ignore = [].concat(['.'], ignore);

    this._compress = {
      level: 6,
      memLevel: 6
    };

    this._init();
  }

  _init() {
    targz.compress({
      src: this._src,
      dest: this._dest,
      tar: {
        ignore: (name) => {
          // console.log('ignore', `(${name})`)
          // return path.extname(name) === '.bin' // ignore .bin files when packing
        }
      },
      gz: this._compress
    }, (err) => {
      if (err) {
        return this.emit('error', err);
      }
      this.emit('done');
    });
  }
}

module.exports = Pack;
