/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

class GenerateList {
  constructor() {
    this.files = {};
    this.mysql = {};
  }

  _addType(item) {
    const getDate = () => {
      let date = [];
      for (let k in item.date) {
        date.push(item.date[k]);
      }
      return date.join('_');
    };

    const formatDate = () => {
      return [
        [item.date.year, item.date.month, item.date.day].join('-'),
        [item.date.hour, item.date.minute].join('-')
      ].join(' ')
    };

    if (!this[item.type].hasOwnProperty(item.name)) {
      this[item.type][item.name] = {}
    }
    this[item.type][item.name][getDate()] = [item.archive, formatDate()].join('|')
  }

  _toArray(type) {
    let arr = [];

    for (let k in this[type]) {
      let dates = [];

      for (let d in this[type][k]) {
        let el = String(this[type][k][d]).split('|');
        dates.push({
          date: el[1],
          file: el[0]
        });
      }

      arr.push({
        name: k,
        dates
      })
    }

    return arr;
  }

  add(item) {
    if (!this.hasOwnProperty(item.type)) {
      this[item.type] = {};
    }
    this._addType(item);
  }

  get() {
    let obj = {};
    for (let k in this) {
      obj[k] = this._toArray(k);
      // obj[k] = this[k];
    }
    return obj;
  }
}

module.exports = GenerateList;
