/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');

class Backup extends EventEmitter {
  constructor() {
    super();
    this.on('error', this.Error);
  }

  Error(e) {
    console.error(e);
    return e;
  }
}

module.exports = Backup;
