/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */
const moment = require('moment');

const EXT = {
  TZ: '.tar.gz',
  SQL: '.sql'
};

class nameItem {
  constructor(name, item = null) {
    this._name = name;
    this._item = item;
    this._ext = EXT.TZ;
  }

  get _getExt() {
    if (this._item && String(this._item.type) === 'mysql') {
      return EXT.SQL
    }
    return EXT.TZ;
  }

  _checkExt(file) {
    if (String(file).includes('mysql')) {
      this._ext = EXT.SQL;
    } else {
      this._ext = EXT.TZ;
    }
    return file;
  }

  /**
   * Генрируем название файла перед сохранением
   * @returns {string}
   */
  generate() {
    return [
      this._name,
      this._item.type || 'unknown',
      moment().format('YYYY-MM-DD_HH-mm')
    ].join('_') + this._getExt;
  }

  parse() {
    let obj = {
      archive: this._name
    };

    this._name = this._checkExt(String(this._name)).replace(this._ext, '').split('_');
    let date = String(this._name[2]).split('-');
    let time = String(this._name[3]).split('-');

    obj = Object.assign(obj, {
      isValidName: this._name.length === 4,
      name: this._name[0] || 'unknown',
      type: this._name[1] || 'unknown',
      date: {
        year: date[0],
        month: date[1],
        day: date[2],
        hour: time[0],
        minute: time[1]
      }
    });

    return obj;
  }
}

module.exports = nameItem;
