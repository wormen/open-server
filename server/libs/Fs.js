/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 класс для работы с файловой системой
 */

'use strict';

const fs = require('fs');
const util = require('util');
const path = require('path');
const fse = require('fs-extra'); // https://www.npmjs.com/package/fs-extra
const rimraf = require('rimraf');
const fileExtension = require('file-extension');
// import Log from "./Log";

const mime = require('mime');

let _Error = (e) => {
  console.error('[Fs]', e);
};

class Fs {
  /**
   * Форматирование данных о размере данных
   * @param length
   * @param format (string | json)
   * @returns {string}
   */
  formatSize(length, format = 'string') {
    let i = 0,
      type = ['B', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb'];

    while ((length / 1000 | 0) && i < type.length - 1) {
      length /= 1024;
      i++;
    }

    let size = Number(length.toFixed(2));
    if (format === 'json') {
      return {
        size,
        type: type[i]
      }
    } else {
      return [size, type[i]].join(' ')
    }
  }

  fileSize(filePath) {
    return this.formatSize(fs.lstatSync(filePath).size, 'json');
  }

  /**
   * Получение полного пути до каталога назначения, без файла в пути
   * @param fullPath
   * @param sep
   */
  getDirInPath(fullPath, sep = path.sep) {
    fullPath = fullPath.split(sep);
    fullPath.splice(fullPath.length - 1, 1);
    return fullPath.join(sep);
  }

  /**
   * Получение названия файла из пути
   * @param fullPath
   * @returns {*}
   */
  getFilenameInPath(fullPath) {
    fullPath = fullPath.split(path.sep);
    return fullPath[fullPath.length - 1];
  }

  /**
   * Рекурсивное создание пустого каталога
   * @param dir
   */
  emptyDir(dir) {
    return new Promise((resolve, reject) => {
      if (fs.existsSync(dir)) {
        return resolve();
      }

      fse.emptyDir(dir)
        .then(resolve)
        .catch(e => {
          _Error(e);
          reject(e);
        })
    });
  }

  getExtension(path) {
    return fileExtension(path);
  }

  getMime(path) {
    return mime.getType(path);
  }

  /**
   * Чтение файла
   * @param filePath
   * @param charset
   * @returns {*}
   */
  readFile(filePath, charset = 'utf8') {
    let readFile = util.promisify(fs.readFile);
    return readFile(filePath, charset);
  }

  readFileSync(filePath, charset = 'utf8') {
    try {
      return fs.readFileSync(filePath, charset);
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Перемещение файла/каталога
   * @param src
   * @param dest
   */
  async move(src, dest) {
    let dir = this.getDirInPath(dest);

    // при необходимости создаем каталог
    if (!fs.existsSync(dir)) {
      await this.emptyDir(dir);
    }
    return fse.move(src, dest);
  }

  async copy(src, dest) {
    let dir = this.getDirInPath(dest);

    // при необходимости создаем каталог
    if (!fs.existsSync(dir)) {
      await this.emptyDir(dir);
    }
    return fse.copy(src, dest);
  }

  async rename(oldPath, newPath) {
    let dir = this.getDirInPath(newPath);

    // при необходимости создаем каталог
    if (!fs.existsSync(dir)) {
      await this.emptyDir(dir);
    }

    let _rm = util.promisify(fs.rename);
    return _rm(oldPath, newPath);
  }

  /**
   * Удаление ресурса
   * @param src
   */
  remove(src) {
    return new Promise((resolve, reject) => {
      if (fs.existsSync(src)) {
        rimraf(src, err => {
          if (err) return reject(err);
          resolve();
        });
      } else {
        resolve(null);
      }
    });
  }

  /**
   * Проверка, является ли элемент списка файлом
   * @param src
   * @returns {*}
   */
  isFile(src) {
    try {
      return fs.lstatSync(src).isFile();
    } catch (e) {
      return false;
    }
  }

  /**
   * Проверка, является ли элемент списка каталогом
   * @param src
   * @returns {*}
   */
  isDirectory(src) {
    try {
      return fs.lstatSync(src).isDirectory();
    } catch (e) {
      return false;
    }
  }

  /**
   * Ассинхронный листинг директории
   * @param dirPath
   * @returns {*}
   */
  dirList(dirPath) {
    let readdir = util.promisify(fs.readdir);
    return readdir(dirPath);
  }

  /**
   * Чтение файла
   * @param filePath
   * @param charset
   * @returns {*}
   */
  readFile(filePath, charset = 'utf8') {
    let readFile = util.promisify(fs.readFile);
    return readFile(filePath, charset);
  }

  /**
   * Запись в файл
   * @param filePath
   * @param data
   * @param opts
   */
  async writeFile(filePath, data, opts = {encoding: 'utf8', overwrite: true}) {
    let dir = this.getDirInPath(filePath);

    // при необходимости создаем каталог
    if (!fs.existsSync(dir)) {
      await this.emptyDir(dir);
    }

    if (opts.overwrite) {
      return fse.outputFile(filePath, data, opts.encoding);
    } else {
      return new Promise((resolve, reject) => {
        fs.appendFile(filePath, data, opts.encoding, (err) => {
          if (err) return reject(err);
          resolve();
        });
      });
    }
  }

  /**
   * Зачитываем JSON файл
   * @param filePath
   * @returns {*}
   */
  readJson(filePath) {
    return fse.readJson(filePath);
  }

  /**
   * Записываем JSON объект в файл
   * @param filePath
   * @param json
   */
  async writeJson(filePath, json = {}) {
    let dir = this.getDirInPath(filePath);

    // при необходимости создаем каталог
    if (!fs.existsSync(dir))
      await this.emptyDir(dir);

    return fse.writeJson(filePath, json);
  }
}

module.exports = new Fs();