/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://electronjs.org/docs/api/tray
 */

const pkg = require('../../../package');
const {app, Menu, Tray} = require('electron');
const {EventEmitter} = require('events');

let tray;

class AppTray extends EventEmitter {
  constructor() {
    super();
    this.isInit = false;
    this.isShowWin = false;
  }

  init(opts = {}) {
    if (this.isInit) {
      return this;
    }
    try {
      tray = new Tray($app.favicon);
      this.setToolTip(pkg.name);

      this.setContextMenu([
        {
          label: 'Показать окно',
          visible: !this.isShowWin,
          click: () => {
            this.emit('show');
          }
        },
        {
          label: 'Настройки',
          visible: !this.isShowWin,
          click: () => {
            this.emit('go', '/setting');
          }
        },
        {type: 'separator'},
        {
          label: 'Выход',
          click: () => {
            app.quit();
          }
        }
      ]);

      this.isInit = true;
      return this;
    } catch (e) {
      console.error(e);
      return this;
    }
  }

  /**
   * https://electronjs.org/docs/api/native-image
   * @param pathImage
   */
  setImage(pathImage = $app.favicon) {
    tray.setImage(pathImage)
  }

  setToolTip(text) {
    tray.setToolTip(text);
  }

  setContextMenu(contextMenu = []) {
    contextMenu = Menu.buildFromTemplate(contextMenu);

    this.on('setShow', state => {
      this.isShowWin = state;

      contextMenu.items[0].enabled = !state;
      tray.setContextMenu(contextMenu);
    });

    // http://electron.atom.io/docs/api/menu
    // https://electronjs.org/docs/api/menu-item
    tray.setContextMenu(contextMenu);
  }
}

// tray.on('click')

module.exports = new AppTray();
