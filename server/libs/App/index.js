/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const fs = require('fs');
const path = require('path');
const {app: index} = require('electron');
const {extend, isObject} = require('../../utils');

// const IPC = require('../../ipc');

class App {
  get cwd() {
    let _path = this.cwdOrig;
    if (String(__dirname).includes(`resources`)) {
      return String(__dirname).split(`${path.sep}resources`)[0];
    }
    return _path;
  }

  get cwdOrig() {
    return index.getAppPath();
  }

  get icon() {
    return path.resolve(this.cwd, 'static', 'icons', '512x512.png');
  }

  get favicon() {
    return path.resolve(this.cwd, 'static', 'icons', 'icon.ico');
  }

  /**
   * Инициализация приложения
   * @returns {Promise}
   */
  init() {
    return new Promise(async (resolve, reject) => {
      const Log = require('../Log');

      // обновление таблиц БД

      // зачитываем конфиг
      Log.info('Loading config ...');

      await (new (require('../os/Setting'))()).getData().then(data => {
        if (isObject(data)) {
          APP_SETTING = extend(APP_SETTING, data, true);
          APP_SETTING.isReady = true;
          Log.info('Config loaded');
          $Faye.sendClient(`system/load-setting`, APP_SETTING);
        } else {
          Log.info('Use default config');
          $Faye.sendClient(`system/load-setting`, APP_SETTING);
        }
        return data;
      }).catch(e => {
        Log.error('Error loaded config', e);
        reject(e);
      });

      resolve();
    });
  }
}

module.exports = App;
module.exports.Tray = require('./Tray');
