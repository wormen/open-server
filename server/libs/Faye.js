/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');
const faye = require('faye');
const deflate = require('permessage-deflate');

const getChannel = (channel) => {
  return '/' + ['client', channel].join('/');
};

const noop = () => {
};

class Faye extends EventEmitter {
  constructor() {
    super();

    this.bayeux = new faye.NodeAdapter({
      mount: '/client',
      timeout: 45
    });
    this.bayeux.addWebsocketExtension(deflate);
  }

  subscribe(channel, cb = noop) {
    this.bayeux.getClient().subscribe(getChannel(channel), cb);
  }

  unsubscribe(channel, cb = noop) {
    this.bayeux.getClient().unsubscribe(getChannel(channel), cb);
  }

  sendClient(channel, data) {
    this.bayeux.getClient().publish(getChannel(channel), data);
  }

  attach(server) {
    this.bayeux.attach(server);
  }
}

module.exports = Faye;
