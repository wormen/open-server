/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');
const si = require('addon-process');

let isRunning = false;

class Process extends EventEmitter {
  constructor() {
    super();

    this._autoPID = 0;
  }

  setAutoPID(val) {
    this._autoPID = val;
  }

  findPidByName(name) {
    return new Promise((resolve) => {
      let pid = null;

      si.processes((data) => {
        data.list.forEach(item => {
          if (item.name === name || item.command === name) {
            pid = item.pid;
          }
        });
        resolve(pid);
      });
    });
  }

  getList(parent = this._autoPID, isTree = false) {

    const treeBuild = (parentPid, list) => {
      let arr = [];

      list.forEach(item => {
        // console.log(item.parentPid, parentPid)
        if (item.parentPid === parentPid) {
          item.children = treeBuild(item.pid, list);
          arr.push(item);
        }
      });

      return arr;
    };

    return new Promise((resolve, reject) => {

      if (parent === 0) {
        return resolve([]);
      }

      si.processes((data) => {
        if (isTree) {
          data.list = treeBuild(parseInt(parent), data.list);
        }
        resolve(data.list);
      });
    });
  }

  autoList(isTree = false) {
    this.getList(this._autoPID, isTree).then(list => {
      this.emit('autoList:data', [].concat([
        {
          name: process.env.OPEN_SERVER.EXEC,
          pid: process.env.OPEN_SERVER.PID
        }
      ], list));
    });

    setTimeout(() => {
      if (isRunning) {
        this.autoList(isTree);
      }
    }, 1e3);
  }

  static start() {
    isRunning = true;
  }

  static stop() {
    isRunning = false;
  }
}

module.exports = Process;
