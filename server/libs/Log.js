/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://www.npmjs.com/package/log4js
 */

const path = require('path');
const log4js = require('log4js');
// const toJSON = require('utils-error-to-json');

let logFile = path.resolve(LOGS_DIR, (process.env.LOG_FILE || 'app-panel.log'));

let params = {
  appenders: {
    everything: {
      type: 'file',
      filename: logFile,
      pattern: '%d{yyyy-MM-dd hh.mm.ss.SSS} %m'
    },
    console: {
      type: 'console',
      layout: {
        type: 'pattern',
        pattern: '%[%d{yyyy-MM-dd hh.mm.ss.SSS} [%p]%] %m'
      }
    }
  },
  categories: {
    default: {
      appenders: ['console', 'everything'],
      level: process.env.DEBUG_LEVEL || 'info'
    }
  }
};

log4js.configure(params);

let log = log4js.getLogger();

class Log {
  static setLogFile(file) {
    params.appenders.everything.filename = file;
    log4js.configure(params);
  }

  static setPattern(channel, pattern) {
    params.appenders[channel].pattern = pattern;
    log4js.configure(params);
  }

  static info(...args) {
    log.info(...args);
  }

  static trace(...args) {
    log.trace(...args);
  }

  static debug(...args) {
    log.debug(...args);
  }

  static warn(...args) {
    log.warn(...args);
  }

  static error(...args) {
    log.error(...args);
  }

  static fatal(...args) {
    log.fatal(...args);
  }
}

module.exports = Log;
