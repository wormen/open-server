/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const fs = require('fs');
const punycode = require('punycode');
const path = require('path');
const Fs = require('../Fs');
const Database = require('../../db');
const OpenServer = require('./index');

class Domains extends OpenServer {
  getListWWW() {
    return Promise.all([
      Fs.dirList(this.wwwPath),
      Database.Domains.findAll({raw: true, attributes: ['name']})
    ]).then(([dirList, baseList]) => {
      const list = new Set();

      dirList.forEach(dir => {
        list.add({name: dir});
        if (dir !== 'localhost') {
          Database.Domains.create({name: dir});
        }
      });

      baseList.forEach(item => {
        if (fs.existsSync(path.resolve(this.wwwPath, item.name))) {
          list.add(item);
        } else {
          Database.Domains.destroy({where: item});
        }
      });

      return {
        www: this.wwwPath,
        list: Array.from(list)
      };
    });
  }

  async save(domain) {
    domain = punycode.toASCII(domain);

    // добавляем домен в БД
    await Database.Domains.create({name: domain});
    // добавдяем DNS запись

    return Fs.emptyDir(path.resolve(this.wwwPath, domain));
  }

  async remove(domain) {
    await Database.Domains.destroy({where: {name: domain}});
    return Fs.remove(path.resolve(this.wwwPath, domain));
  }
}

module.exports = Domains;
