/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const path = require('path');
const OpenServer = require('./index');
const Init = require('./Init');
const Fs = require('../Fs');

const fileEOL = `\r\n`;

class Ftp extends OpenServer {
  constructor() {
    super();

    this._file = path.resolve(this.profilesPath, `{profile}_ftpusers.txt`)
  }

  _profilePath(name) {
    return this._file.replace('{profile}', name);
  }

  getUsers() {
    return new Promise(async (resolve, reject) => {
      try {
        const init = await (new Init()).getData();
        let fd = Fs.readFileSync(this._profilePath(init.main.profile));

        let arr = [];
        fd.split(`\n`).forEach(line => {
          if (line.trim().length > 0) {
            let item = line.trim().split(';');
            arr.push({
              login: item[1],
              password: item[2],
              mode: item[3],
              dir: item[4],
              enabled: item[0] === ':'
            });
          }
        });

        resolve(arr);
      } catch (e) {
        reject(e);
      }
    });
  }

  saveUsers(list = []) {
    return new Promise(async (resolve, reject) => {

      let arr = [];
      list.forEach(item => {
        arr.push([
          item.enabled ? ':' : '.',
          item.login,
          item.password,
          item.mode,
          item.dir
        ].join(';'))
      });

      const init = await (new Init()).getData()
        .catch(reject);

      const file = this._profilePath(init.main.profile);

      Fs.writeFile(file, arr.join(fileEOL) + fileEOL)
        .then(resolve)
        .catch(reject)
    });
  }
}

module.exports = Ftp;
