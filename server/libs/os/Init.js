/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Класс для работы с файлом init.ini
 */

const fs = require('fs');
const ini = require('ini');
const path = require('path');
const OpenServer = require('./index');
const Fs = require('../Fs');

class Init extends OpenServer {
  constructor() {
    super();

    this._file = path.resolve(this.userdataPath, 'init.ini');
  }

  get exists() {
    return fs.existsSync(this._file);
  }

  getData() {
    return new Promise(async (resolve, reject) => {
      try {
        const init = ini.parse(Fs.readFileSync(this._file));
        resolve(init);
      } catch (e) {
        reject(e);
      }
    });
  }

  saveData(json = {}) {
    return this.getData().then(init => {
      init.main = Object.assign(init.main, json.main || json);
      return Fs.writeFile(this._file, ini.stringify(init));
    });
  }
}

module.exports = Init;
