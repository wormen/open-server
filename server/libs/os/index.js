/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const fs = require('fs');
const ini = require('ini');
const path = require('path');
const _ = require('lodash');
const {EventEmitter} = require('events');
const {spawn} = require('child_process');
const Database = require('../../db');
const Process = require('../Process');
const Fs = require('../Fs');

// Пути Open Server
let PATH_CONST = {
  REALPROGDIR: ROOT_DIR, // Реальный путь до папки с OSPanel (обратный слеш "\")
  PROGDIR: '', // Генерируемый путь до папки с OSPanel с учетом виртуального диска (обратный слеш "\")
  SPROGDIR: '', // Генерируемый путь до папки с OSPanel с учетом виртуального диска (слеш "/")
  DPROGDIR: '', // Генерируемый путь до папки с OSPanel с учетом виртуального диска (двойной обратный слеш "\\")
  DSPROGDIR: '', // Генерируемый путь до папки с OSPanel с учетом виртуального диска (двойной слеш "//")
  REALSITEDIR: '', // Реальный путь до корневой папки доменов (обратный слеш "\")
  SITEDIR: '', // Генерируемый путь до корневой папки доменов с учетом виртуального диска (обратный слеш "\")
  SSITEDIR: '', // Генерируемый путь до корневой папки доменов с учетом виртуального диска (слеш "/")
  DISK: '', // Буква диска из генерируемого пути до папки с OSPanel с учетом виртуального диска (только буква)
  OSDISK: '', // Буква диска из реального пути до папки с OSPanel (только буква)
  SYSDISK: '' // Системный диск Windows (только буква)
};

let isValidRoot = false; // флаг для указывающий, что корневой каталог указан верно

class OpenServer extends EventEmitter {
  static async init(isRestartOS = false) {
    return new Promise(async (resolve, reject) => {
      const joinEnv = (data = {}) => {
        if (data.hasOwnProperty('ROOT_DIR')) {
          PATH_CONST.REALPROGDIR = data.ROOT_DIR;
        }
        process.env.OPEN_SERVER = Object.assign({}, process.env.OPEN_SERVER, data);
      };

      let _path = path.resolve((new OpenServer()).userdataPath, 'init.ini');
      isValidRoot = fs.existsSync(_path);
      joinEnv({
        VALID_ROOT_DIR: isValidRoot
      });

      // зачитываем корневой путь из конфига до OS
      await getKey('OS').then(val => {
        if (val && _.isObject(val) && val.hasOwnProperty('dir')) {
          joinEnv({
            EXEC: val.file || '',
            ROOT_DIR: val.dir,
            SITEDIR: val.www || ''
          });
        }
      }).catch(reject);

      await getKey('sys').then(val => {
        if (_.isObject(val) && val.hasOwnProperty('backupDir')) {
          joinEnv({
            BACKUP_DIR: val.backupDir,
            SYSTEM_NOTIFY: Boolean(val.nativeNotify)
          });
        }
      }).catch(reject);

      if (isValidRoot) {
        try {
          const init = await getIniData(_path);
          joinEnv({PROFILE: init.main.profile});

          const {main} = await getProfileData(init.main.profile);
          let ACTIVE_MODULE = {};
          let ACTIVE_DRIVER = {};

          for (let k in main) {
            switch (k) {
              case 'mysql_driver':
              case 'pg_driver':
              case 'mongo_driver':
                if (main[k] !== 'none') {
                  ACTIVE_MODULE[k.toUpperCase()] = true;
                }
                break;
            }
            if (String(k).includes('driver')) {
              ACTIVE_DRIVER[String(k)
                .replace(/_driver/, '')
                .replace(/driver/, '')
                .toUpperCase()] = main[k] === 'none' ? false : main[k];
            }
          }

          joinEnv({ACTIVE_MODULE, ACTIVE_DRIVER});
        } catch (e) {
        }
      }

      try {
        const _Process = new Process();

        // проверяем, запущен ли OS, получаем PID
        if (process.env.OPEN_SERVER.hasOwnProperty('EXEC') && String(process.env.OPEN_SERVER.EXEC).length > 0) {
          await _Process.findPidByName(process.env.OPEN_SERVER.EXEC)
            .then(pid => {
              if (_.isNumber(pid)) {
                joinEnv({
                  PID: pid
                });
              }
            })
            .catch(reject);
        }
      } catch (e) {
        console.error(e)
      }

      resolve();

      // автоматическое перечитывание конфига
      if (!isRestartOS) {
        setTimeout(() => {
          OpenServer.init()
        }, 1e3);
      }
    });
  }

  static setVar(key, val) {
    PATH_CONST[key] = val;
  }

  /**
   * Запустить OS
   * @returns {*}
   */
  static start() {
    return osSetState('start');
  }

  /**
   * Остановить OS
   * @returns {*}
   */
  static stop() {
    return osSetState('stop');
  }

  /**
   * Перезапустить OS
   * @returns {*}
   */
  static restart() {
    return osSetState('restart');
  }

  /**
   * Выход из OS
   * @returns {*}
   */
  static exit() {
    return osSetState('exit');
  }

  getPath(type = 'REALPROGDIR') {
    return String(type).includes('REALPROGDIR') ? process.env.OPEN_SERVER.ROOT_DIR : PATH_CONST[type];
  }

  get userdataPath() {
    return path.resolve(this.getPath('REALPROGDIR'), 'userdata');
  }

  get modulesDir() {
    return path.resolve(this.getPath('REALPROGDIR'), 'modules');
  }

  get logPath() {
    return path.resolve(this.userdataPath, 'logs');
  }

  get profilesPath() {
    return path.resolve(this.userdataPath, 'profiles');
  }

  get emailPath() {
    return path.resolve(this.userdataPath, 'temp', 'email'); // todo путь получаем из настроек
  }

  get wwwPath() {
    return process.env.OPEN_SERVER.SITEDIR || path.resolve(this.getPath(), 'domains'); // todo путь получаем из настроек
  }
}

module.exports = OpenServer;
module.exports.getKey = getKey;
module.exports.getIniData = getIniData;
module.exports.getProfileData = getProfileData;

function osSetState(state) {
  state = String(state).toLowerCase();

  return new Promise((resolve, reject) => {
    if (!fs.existsSync(process.env.OPEN_SERVER.ROOT_DIR)) {
      return reject(new Error('Invalid ROOT_DIR for OpenServer'));
    }

    const isLaunched = () => {
      return process.env.OPEN_SERVER.hasOwnProperty('PID');
    };

    let exec = path.resolve(process.env.OPEN_SERVER.ROOT_DIR, process.env.OPEN_SERVER.EXEC);

    if (state === 'restart' && !isLaunched()) {
      state = 'start';
    }

    if (state === 'start' && isLaunched()) {
      state = 'restart';
    }

    console.log(`Open Server isLaunched: ${isLaunched()}, set state:`, state);
    console.log(`"${exec}"`, `/${state}`);

    try {
      let app = spawn(`"${exec}"`, [`/${state}`], {
        env: process.env,
        detached: true,
        shell: true,
        stdio: [null, process.stdout, process.stderr]
      });

      process.env.OPEN_SERVER.PID = app.pid;

      if (app && app.stderr) {
        app.stderr.on('data', (data) => {
          console.error(data.toString());
        });
      }

      app.on('close', (code) => {
        delete process.env.OPEN_SERVER.PID;
        if (code === 0) {
          resolve();
        } else {
          reject(new Error('Error launched Open Server'));
        }
      });
    } catch (e) {
      reject(e);
    }
  });
}

function getKey(key) {
  return Database.Setting.findOne({
    where: {key},
    attributes: ['value']
  }).then(val => {
    if (val && val.value && isJson(val.value)) {
      val = JSON.parse(val.value);
    }
    return val;
  });
}

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function getIniData(file) {
  return new Promise(async (resolve, reject) => {
    try {
      const init = ini.parse(Fs.readFileSync(file));
      resolve(init);
    } catch (e) {
      reject(e);
    }
  });
}

function getProfileData(name = process.env.OPEN_SERVER.PROFILE) {
  return getIniData(path.resolve((new OpenServer()).profilesPath, `${name}.ini`));
}
