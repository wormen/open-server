/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const fs = require('fs');
const path = require('path');
const Fs = require('../Fs');
const OpenServer = require('./index');

class MySQL extends OpenServer {
  getList() {
    return new Promise(async (resolve, reject) => {
      try {
        const profile = await OpenServer.getProfileData();
        // console.log(profile)
        const dir = path.resolve(this.userdataPath, profile.main.mysql_driver);
        // console.log(dir)
        if (!fs.existsSync(dir)) {
          return resolve([]);
        }
        Fs.dirList(dir)
          .then(list => {
            return list.filter(item => {
              if ([
                'mysql', 'performance_schema', 'sys',
                'information_schema', '#mysql50#.rocksdb'
              ].includes(item)) {
                return false;
              }
              return Fs.isDirectory(path.resolve(dir, item));
            });
          })
          .then(list => {
            list = list.map(item => {
              item = String(item);
              if (item.includes('@')) {
                for (let u of item.match(/\@[a-zA-Z0-9]{4}/g)) {
                  item = item.replace(u, JSON.parse(`"\\u${u.replace(/\@/, '')}"`));
                }
              }
              return {name: item};
            });

            resolve(list);
          })
          .catch(reject);
      } catch (e) {
        console.error(e);
        resolve([]);
      }
    });
  }
}

module.exports = MySQL;
