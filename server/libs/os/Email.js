/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const path = require('path');
const Fs = require('../Fs');
const OpenServer = require('./index');
const {MailParser} = require('mailparser');

class Email extends OpenServer {
  getList() {
    return Fs.dirList(this.emailPath);
  }

  viewFile(fileName) {
    return new Promise((resolve, reject) => {
      try {
        const src = Fs.readFileSync(path.resolve(this.emailPath, fileName));

        const mailparser = new MailParser({
          // streamAttachments: true,
          showAttachmentLinks: true
        });

        mailparser.on("end", (mail_object) => {
          if (mail_object.hasOwnProperty('text')) {
            mail_object.text = mail_object.text
              .replace(/\n\r/g, `<br/>`)
              .replace(/\n/g, `<br/>`);
          }

          const insertImage = (text, item) => {
            const img = `data:${item.contentType};${item.transferEncoding},${item.content.toString(item.transferEncoding)}`;
            const cid = `cid:${item.contentId}`;

            const _replaceId = (_text, _cid) => {
              _text = _text.replace(_cid, img);
              if (_text.includes(_cid)) {
                _text = _replaceId(_text, _cid);
              }
              return _text;
            };

            text = String(text)
              .replace(new RegExp(`<${item.generatedFileName}>`, 'g'), `<img src="${img}"/>`)
              .replace(new RegExp(`&lt;${item.generatedFileName}&gt;`, 'g'), `<img src="${img}"/>`);

            return _replaceId(text, cid);
          };

          if (mail_object.hasOwnProperty('attachments') && mail_object.attachments.length > 0) {
            mail_object.attachments.forEach(item => {
              if (mail_object.hasOwnProperty('text') && item.contentType.includes('image')) {
                mail_object.text = insertImage(mail_object.text, item)
              }

              if (mail_object.hasOwnProperty('html') && item.contentType.includes('image')) {
                mail_object.html = insertImage(mail_object.html, item)
              }
            });
          }

          delete mail_object.attachments;

          resolve({
            parsed: mail_object,
            source: src
          });

        });

        mailparser.write(src);
        mailparser.end();

      } catch (e) {
        console.error(e);
        reject(e);
      }
    });
  }

  remove(fileName) {
    return Fs.remove(path.resolve(this.emailPath, fileName));
  }
}

module.exports = Email;
