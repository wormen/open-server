/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const OpenServer = require('./index');
const Database = require('../../db');
const {isJson} = require('../../utils');
const Init = require('./Init');
const Profile = require('./Profile');
const Fs = require('../Fs');

class Setting extends OpenServer {
  getData() {
    let obj = {};
    return Database.Setting.all().then(async (list) => {
      list.forEach(({key, value}) => {
        obj[key] = isJson(value) ? JSON.parse(value) : value;
      });
      let profile = 'Default';
      let profiles = [profile];
      try {
        let dir = path.resolve(obj.OS.dir, 'userdata', 'profiles');
        if (String(obj.OS.dir).length > 0 && fs.existsSync(dir)) {
          await Fs.dirList(dir).then(list => {
            profiles = list.filter(item => {
              return String(item).includes('.ini');
            }).map(item => {
              return String(item).replace(/\.ini/, '');
            });
          });
        }

        if ((new Init()).exists) {
          const $init = new Init();
          await $init.getData().then(data => {
            profile = data.main.profile;
          });
        }
      } catch (e) {
      }
      return {
        profile,
        profiles,
        settings: obj
      };
    });
  }

  async save(data = {}, isRestartOS = true) {
    const defaultProfile = 'Default';
    let profile = data.profile || defaultProfile;

    const saveKey = (obj, profile = defaultProfile) => {
      obj.profile = profile;
      return Database.upsert(Database.Setting, obj, {key: obj.key});
    };

    const boolToStr = (val) => {
      return String(Number(val));
    };

    const findPath = (str) => {
      str = String(str)
        .replace(/'"/g, `"`)
        .replace(/"'/g, `"`)
        .replace(/"/g, ``);
      return str;
    };

    const saveIni = (_data, profileName) => {
      return new Promise(async (resolve, reject) => {
        try {
          const $init = new Init();
          if ($init.exists) {
            let init = await $init.getData();

            const $profile = new Profile(profileName);
            let profile = await $profile.getData();

            init.main = Object.assign(init.main, {
              autostart: boolToStr(_data.general.autostart),
              clearlogs: boolToStr(_data.general.clearlogs),
              checkupdate: boolToStr(_data.general.checkupdate),
              wait: String(_data.general.wait)
            });

            profile.main = Object.assign(profile.main, {
              allow: boolToStr(_data.server.allow),
              astart: boolToStr(_data.server.astart),
              debugmode: boolToStr(_data.server.debugmode),
              selfhosts: boolToStr(_data.server.selfhosts)
            });

            for (let k in profile.main) {
              profile.main[k] = findPath(profile.main[k]);
            }

            profile.ftp = Object.assign(profile.ftp, {
              ftp: boolToStr(_data.server.ftp.isStartServer),
              ftpcommandtimeout: String(_data.server.ftp.commandtimeout),
              ftpconnecttimeout: String(_data.server.ftp.connecttimeout)
            });

            const P = _data.server.ports;
            profile.ports = Object.assign(profile.ports, {
              mysqlport: P.MySQL,
              postgresqlport: P.Postgres,
              mongodbport: P.MongoDB,
              httpport: P.HTTP,
              httpsport: P.HTTPS,
              httpbackport: P.Backend,
              ftpport: P.FTP,
              sftpport: P.FTPS,
              phpport: P.PHP,
              redisport: P.Redis,
              memcacheport: P.Memcache
            });

            await $init.saveData(init);
            await $profile.saveData(profile);

            return resolve(init.main.profile);
          }

          resolve(profile);
        } catch (e) {
          reject(e);
        }
      });
    };

    return new Promise(async (resolve, reject) => {
      let resave = String(data.settings.OS.dir).length >= 5;

      // console.log(data)
      // return resolve();

      if ((new Init()).exists) {
        profile = await saveIni(data.settings, data.profile || defaultProfile).catch(reject);
      }

      // сохраняем данные в свою БД
      let arr = [];
      for (let k in data.settings) {
        arr.push(saveKey({
          key: k,
          value: _.isObject(data.settings[k]) ? JSON.stringify(data.settings[k]) : data.settings[k]
        }, profile))
      }

      Promise.all(arr)
        .then(async () => {
          if (resave) {
            await saveIni(data.settings, data.profile || defaultProfile);

            resolve();

            let _path = path.resolve(data.OS.dir);
            process.env.OPEN_SERVER.ROOT_DIR = _path;
            OpenServer.setVar('REALPROGDIR', _path);

            await OpenServer.init(true);

            // перезагрузка OS
            if (isRestartOS) {
              OpenServer.restart();
            }
          } else {
            resolve();
          }
        })
        .catch(reject);
    });
  }
}

module.exports = Setting;
