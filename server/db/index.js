/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://github.com/louischatriot/nedb
 */

const path = require('path');
const Sequelize = require('sequelize');

let DB = null;

class Database {
  constructor() {
    this._ext = '.db';
    this._init();
  }

  _init() {
    const storage = path.resolve(DATA_DIR, `osp${this._ext}`);
    DB = new Sequelize('os-panel', null, null, {
      dialect: 'sqlite',
      logging: false,
      define: {
        underscored: true,
        freezeTableName: true,
        charset: 'utf8',
        dialectOptions: {
          collate: 'utf8_general_ci'
        },
        timestamps: false
      },
      storage,
      operatorsAliases: Sequelize.Op // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    });
  }

  // --------- функции БД ---------
  static query(sql, opts) {
    return DB.query(sql, Object.assign({raw: true, plain: true}, opts));
  }

  static upsert(model, data = {}, where = {}) {
    return new Promise((resolve, reject) => {
      model.create(data)
        .then(resolve)
        .catch(e => {
          if (e.name === 'SequelizeUniqueConstraintError') {
            return model.update(data, {where})
              .then(resolve)
              .catch(reject)
          } else {
            reject(e);
          }
        });
    });
  }

  // --------- Модели ---------

  static get SshConnettions() {
    const model = DB.define('ssh_connections', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ''
      },
      login: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ''
      },
      password: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ''
      },
      host: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ''
      },
      port: {
        type: Sequelize.INTEGER,
        defaultValue: 22
      },
      useKey: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      keyType: {
        type: Sequelize.TEXT,
        defaultValue: 'putty'
      },
      keyName: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ''
      },
      data: { // данные в формате JSON
        type: Sequelize.TEXT,
        defaultValue: '{}'
      }
    }, {
      updateOnDuplicate: true,
      tableName: 'ssh_connections'
    });

    model.sync();

    return model;
  }

  static get Setting() {
    const model = DB.define('setting', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      key: {
        type: Sequelize.TEXT,
        allowNull: false,
        unique: true
      },
      value: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      profile: {
        type: Sequelize.TEXT,
        allowNull: false
      }
    }, {
      updateOnDuplicate: true,
      tableName: 'setting'
    });

    model.sync();

    return model;
  }

  static get Domains() {
    const model = DB.define('domains', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false,
        unique: true
      }
    }, {
      tableName: 'domains'
    });

    model.sync();

    return model;
  }

  static get Cron() {
    const model = DB.define('cron', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      cmd: {
        type: Sequelize.TEXT,
        defaultValue: ''
      },
      type: {
        type: Sequelize.TEXT,
        defaultValue: ''
      },
      comment: {
        type: Sequelize.TEXT,
        defaultValue: ''
      },
      time: {
        type: Sequelize.JSON
      },
      genetated: {
        type: Sequelize.TEXT,
        defaultValue: ''
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }
    }, {
      tableName: 'cron'
    });

    model.sync();

    return model;
  }
}

module.exports = Database;

new Database();
