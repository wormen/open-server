/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');
const Cpu = require('./Cpu');
const Drives = require('./Drives');
const Memory = require('./Memory');

class Monitoring extends EventEmitter {
  constructor() {
    super();
    this._init();
  }

  _init() {
    (new Cpu()).on('data', (data) => {
      this.emit(`cpu`, data);
    });

    (new Memory()).on('data', (data) => {
      this.emit(`memory`, data);
    });

    (new Drives()).on('data', (data) => {
      this.emit(`disk`, data);
    });
  }

  static start() {
    Cpu.start();
    Drives.start();
    Memory.start()
  }

  static stop() {
    Cpu.stop();
    Drives.stop();
    Memory.stop()
  }
}

module.exports = Monitoring;
