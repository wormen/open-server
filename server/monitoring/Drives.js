/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 http://blog.wadmin.ru/2012/04/wmic-examples/
 */

const {EventEmitter} = require('events');

const si = require('addon-drives');
const disk = require('diskusage');
let isRinning = false;

class Drives extends EventEmitter {
  constructor() {
    super();
    this._pause = 1e3;

    this._getData();
  }

  _getData() {
    const driveInfo = (id, size, label) => {
      let obj = {
        DeviceID: id,
        VolumeName: label,
        Size: size.total,
        FreeSpace: size.free
      };

      obj.freePercent = Number((Number(obj.FreeSpace) * 100 / Number(obj.Size)).toFixed(2));
      obj.usePercent = Number((100 - obj.freePercent).toFixed(2));

      return obj;
    };

    si.blockDevices(list => {
      let arr = [];
      list.forEach(item => {
        if (item.physical === 'HDD') {
          try {
            let size = disk.checkSync(item.identifier);
            arr.push(driveInfo(item.identifier, size, item.label));
          } catch (err) {
            console.error(err);
          }
        }
      });

      this.emit('data', arr);
    });

    setTimeout(() => {
      if (isRinning) {
        this._getData()
      }
    }, this._pause);
  }

  setPause(val) {
    this._pause = val;
  }

  static start() {
    isRinning = true;
  }

  static stop() {
    isRinning = false;
  }
}

module.exports = Drives;
