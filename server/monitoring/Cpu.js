/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');
const {cpus} = require('os');
let isRinning = false;

class CoreInfo {
  constructor(data, num, isHosted = false) {
    this._num = num + 1;

    if (isHosted) {
      this._total = parseInt(data.user) + parseInt(data.system) + parseInt(data.idle);
      this._sys = parseInt(data.system);
      this._user = parseInt(data.user);
      this._free = parseInt(data.idle);
      this._use = parseInt(data.sys) + parseInt(data.user);

    } else {
      let total = 0;
      for (let T in data.times) {
        total += parseFloat(data.times[T]);
      }

      this._total = total;
      this._sys = data.times.sys;
      this._user = data.times.user;
      this._free = data.times.idle;
      this._use = (data.times.sys + data.times.user);
    }
  }

  get(start) {
    let dif = {
      num: this._num
    };
    let free = (this._free - start._free);
    let total = (this._total - start._total);
    dif.use = Number(((1 - free / total) * 100).toFixed(2));
    return dif;
  }
}

class NativeCpu extends EventEmitter {
  constructor() {
    super();

    this._total = {
      free: 0,
      use: 0
    };

    this._pause = 1e3;

    this._getData();
  }

  _reset() {
    this._total = {
      free: 0,
      use: 0
    };
  }

  _setUse(val, free) {
    val = Number(val.toFixed(2));
    free = Number(free.toFixed(2));

    let setVal = () => {
      this._total.use = val;
      this._total.free = free;
    };

    if (this._total.use === 0) {
      setVal();
    } else if (val > this._total.use) {
      setVal();
    }
  }

  _getData() {
    let cores = [];

    this._reset();

    cpus().forEach((core, i) => {
      cores[i] = new CoreInfo(core, i);
    });

    if (this.prevData) {
      let realCores = [];
      for (let c in cores) {
        let dif = cores[c].get(this.prevData[c]);
        this._setUse(parseFloat(dif.use), 100 - parseFloat(dif.use));
        realCores.push(dif);
      }
      super.emit('data', {
        cores: realCores,
        total: this._total
      });
    }

    this.prevData = cores;

    setTimeout(() => {
      if (isRinning) {
        this._getData()
      }
    }, this._pause);
  }

  setPause(val) {
    this._pause = val;
  }

  static start() {
    isRinning = true;
  }

  static stop() {
    isRinning = false;
  }
}

module.exports = NativeCpu;
