/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');
const {freemem, totalmem} = require('os');
let isRinning = false;

class NativeMemory extends EventEmitter {
  constructor() {
    super();

    this._pause = 1e3;

    this._getData();
  }

  _getData() {
    let freeMem = freemem();
    let totalMem = totalmem();
    let freePercent = Number((freeMem / totalMem * 100).toFixed(2));
    let usePercent = 100 - freePercent;

    super.emit('data', {
      freeMem,
      totalMem,
      usePercent,
      freePercent
    });

    setTimeout(() => {
      if (isRinning) {
        this._getData()
      }
    }, this._pause);
  }

  setPause(val) {
    this._pause = val;
  }

  static start() {
    isRinning = true;
  }

  static stop() {
    isRinning = false;
  }
}

module.exports = NativeMemory;
