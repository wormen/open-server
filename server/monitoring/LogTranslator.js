/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: olegbogdanov86@gmail.com
 */

"use strict";

const fs = require('fs');
const {EventEmitter} = require('events');

export default class Log extends EventEmitter {
  constructor() {
    super();

    var dirOS = $Core.Fs.readJsonSync($path.join(ConfigDir, 'os-dir.txt'));
    this.dirOS = dirOS ? dirOS : null;

    this.on('error', e => {
      console.error(e);
    });

    this.on('info', (str) => {
      console.log(str);
    });

    this.on('new_log', (stream, level, msg) => {
      if (msg && msg.length >= 3) {
        msg = String(msg).trim();
        let obj = {stream, level, msg};
        $Faye.sendClient(`system/logs`, obj);
      }
    });
  }

  // список файлов для наблюдения
  get getFilesPath() {
    return $Core.Fs.readJsonSync($path.join(ConfigDir, 'harvester.json'));
  }

  // /**
  //  * Обработка событий
  //  * @param event
  //  * @param string
  //  */
  // emit(event, string) {
  //   $Core.emit(event, string);
  // }

  /**
   * Наблюдаем за изменениями файла
   * @param path
   * @param stream
   * @param level
   * @returns {{close, pause}|string|*}
   */
  watchFile(path, stream, level) {
    var currSize, watcher,
      _this = this;

    if (!fs.existsSync(path)) {
      // $Core.emit('error', "File doesn't exist: '" + path + "'");
      setTimeout(() => {
        return _this.watchFile(path, stream, level);
      }, 1000);
      return;
    }

    // _this.emit('info', "Watching file: '" + path + "'");
    currSize = fs.statSync(path).size;

    return watcher = fs.watch(path, (event, filename) => {
      if (event === 'rename') {
        watcher.close();
        _this.watchFile(path, stream, level);
      }

      if (event === 'change') {
        return fs.stat(path, (err, stat) => {
          _this.readLogs(path, stat.size, currSize, stream, level);
          return currSize = stat.size;
        });
      }
    });
  }

  /**
   * Зачитываем измеенния файла
   * @param path
   * @param curr
   * @param prev
   * @param stream
   * @param level
   * @returns {*}
   */
  readLogs(path, curr, prev, stream, level) {
    if (curr < prev) return;

    let rstream = fs.createReadStream(path, {
      encoding: 'utf8',
      start: prev,
      end: curr
    });

    return rstream.on('data', (data) => {
      let line;
      let lines = data.split("\n");
      // let _results = [];

      for (let _i = 0, _len = lines.length; _i < _len; _i++) {
        line = lines[_i];

        if (line) {
          this.emit('new_log', stream, level, line)
          // _results.push();
        }
      }

      // return _results;
    });
  };

  checkFile(file, stream, level) {
    if (!this.dirOS || !file) return;


    file = $path.join(this.dirOS, 'userdata', 'logs', file);

    if (fs.existsSync(file))
      this.watchFile(file, stream, level);
  }

  run() {
    var list = this.getFilesPath;
    for (let stream in list) {

      for (let S of list[stream])
        this.checkFile(S.file, stream, S.level);
    }

  }
}

Log = new Log;

// Log.run();
