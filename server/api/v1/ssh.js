const {Router} = require('express');
const Ssh = require('../../libs/Ssh');
const IPC = require('../../ipc');

const router = Router();
const MU = '/ssh';

const ssh = new Ssh();

// router.get(`${MU}/getList`, (req, res) => {
//   (new Ftp()).getUsers()
//     .then(list => res.json(list))
//     .catch(e => res.sendStatus(500));
// });

router.get(`${MU}/getConnections`, (req, res) => {
  ssh.getConnections()
    .then(list => res.json(list))
    .catch(e => res.sendStatus(500));
});

router.get(`${MU}/openTerminal/:id`, (req, res) => {
  ssh.openTerminal(req.params.id)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

router.get(`${MU}/openPuttyTerminal/:id`, (req, res) => {
  ssh.openPuttyTerminal(req.params.id)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

router.put(`${MU}/terminalCommand/:id`, (req, res) => {
  ssh.terminalCommand(req.params.id, req.body.cmd)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/closeTerminal/:id`, (req, res) => {
  ssh.closeTerminal(req.params.id)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/save`, (req, res) => {
  ssh.save(req.body)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.put(`${MU}/update/:id`, (req, res) => {
  ssh.update(req.params.id, req.body)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/remove/:id`, (req, res) => {
  ssh.remove(req.params.id)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

// --- SSH ключи ---
router.get(`${MU}/getKeys`, (req, res) => {
  ssh.getKeys()
    .then(list => res.json(list))
    .catch(e => res.sendStatus(500));
});

router.get(`${MU}/getKey/:name`, (req, res) => {
  ssh.getKey(req.params.name)
    .then(data => res.send(data))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/saveKey`, (req, res) => {
  ssh.saveKey(req.body.name, req.body.content)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/removeKey/:name`, (req, res) => {
  ssh.removeKey(req.params.name)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

module.exports = router;

IPC.on('ssh::getConnections', (event, val) => {
  ssh.getConnections()
    .then(list => {
      event.sender.send('ssh::getConnections:ok', list.map(({dataValues}) => {
        return dataValues;
      }));
    })
    .catch(e => {
      console.error(e);
      event.sender.send('ssh::getConnections:error');
    });
});

IPC.on('ssh::removeConnection', (event, id) => {
  ssh.remove(id)
    .then(() => {
      event.sender.send('ssh::removeConnection:ok');
    })
    .catch(e => {
      console.error(e);
      event.sender.send('ssh::removeConnection:error');
    });
});

IPC.on('ssh::getKeys', (event, val) => {
  ssh.getKeys()
    .then(list => {
      event.sender.send('ssh::getKeys:ok', list);
    })
    .catch(e => {
      console.error(e);
      event.sender.send('ssh::getKeys:error');
    });
});

IPC.on('ssh::removeKey', (event, name) => {
  ssh.removeKey(name)
    .then(list => {
      event.sender.send('ssh::removeKey:ok', list);
    })
    .catch(e => {
      console.error(e);
      event.sender.send('ssh::removeKey:error');
    });
});

IPC.on('openPuttyTerminal', (event, id) => {
  ssh.openPuttyTerminal(id);
});
