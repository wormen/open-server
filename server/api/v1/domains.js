const {Router} = require('express');
const Domains = require('../../libs/os/Domains');
const {exec} = require('child_process');
const path = require('path');
const IPC = require('../../ipc');

const router = Router();
const MU = '/domains';

router.get(`${MU}/getList`, (req, res) => {
  (new Domains()).getListWWW()
    .then(list => res.json(list))
    .catch(e => {
      console.error(e);
      res.sendStatus(500)
    });
});

router.post(`${MU}/open-dir`, (req, res) => {
  openDir(req.body)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/save`, (req, res) => {
  (new Domains()).save(req.body.name)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/remove/:name`, (req, res) => {
  (new Domains()).remove(req.params.name)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

module.exports = router;

IPC.on('domains::getList', (event, val) => {
  (new Domains()).getListWWW()
    .then(list => {
      event.sender.send('domains::getList:ok', list)
    })
    .catch(e => {
      console.error(e);
      event.sender.send('domains::getList:error');
    });
});

IPC.on('domains::create', (event, val) => {
  (new Domains()).save(val.name)
    .then(() => {
      event.sender.send('domains::create:ok');
    })
    .catch(e => {
      console.error(e);
      event.sender.send('domains::create:error');
    });
});

IPC.on('domains::openDir', (event, val) => {
  openDir(val);
});

IPC.on('domains::remove', (event, name) => {
  (new Domains()).remove(name)
    .then(() => {
      event.sender.send('domains::remove:ok')
    })
    .catch(e => {
      console.error(e);
      event.sender.send('domains::remove:error');
    });
});

function openDir(data) {
  return new Promise((resolve, reject) => {
    exec(`start "" "${path.resolve(data.root, data.dir)}"`, (err) => {
      if (err) {
        return reject(err);
      }
      resolve();
    })
  });
}
