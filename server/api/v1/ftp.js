const {Router} = require('express');
const Ftp = require('../../libs/os/Ftp');
const IPC = require('../../ipc');

const router = Router();
const MU = '/ftp';

router.get(`${MU}/getList`, (req, res) => {
  (new Ftp()).getUsers()
    .then(list => res.json(list))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/save`, (req, res) => {
  save(req.body.users)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

module.exports = router;

IPC.on('ftp::getList', (event, val) => {
  (new Ftp()).getUsers()
    .then(list => {
      event.sender.send('ftp::getList:ok', list);
    })
    .catch(e => {
      console.error(e);
      event.sender.send('ftp::getList:error');
    });
});

IPC.on('ftp::save', (event, val) => {
  save(val.users)
    .then(() => {
      event.sender.send('ftp::save:ok');
    })
    .catch(e => {
      console.error(e);
      event.sender.send('ftp::save:error');
    });
});

function save(users) {
  return (new Ftp()).saveUsers(users);
}
