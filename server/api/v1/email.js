const {Router} = require('express');
const Email = require('../../libs/os/Email');
const IPC = require('../../ipc');

const router = Router();
const MU = '/email';

router.get(`${MU}/getList`, (req, res) => {
  (new Email()).getList()
    .then(list => res.json(list))
    .catch(e => res.sendStatus(500));
});

router.get(`${MU}/getItem/:name`, (req, res) => {
  (new Email()).viewFile(req.params.name)
    .then(data => res.json(data))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/remove/:name`, (req, res) => {
  (new Email()).remove(req.params.name)
    .then(() => res.send(200))
    .catch(e => res.sendStatus(500));
});

module.exports = router;

IPC.on('email::getList', (event, val) => {
  (new Email()).getList()
    .then(list => {
      event.sender.send('email::getList:ok', list);
    })
    .catch(e => {
      console.error(e);
      event.sender.send('email::getList:error');
    });
});

IPC.on('email::getItem', (event, name) => {
  (new Email()).viewFile(name)
    .then(data => {
      event.sender.send('email::getItem:ok', data);
    })
    .catch(e => {
      console.error(e);
      event.sender.send('email::getItem:error');
    });
});

IPC.on('email::remove', (event, name) => {
  (new Email()).remove(name)
    .then(() => {
      event.sender.send('email::remove:ok');
    })
    .catch(e => {
      console.error(e);
      event.sender.send('email::remove:error');
    });
});
