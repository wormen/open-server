const {Router} = require('express');
const Setting = require('../../libs/os/Setting');
const Log = require('../../libs/Log');
const IPC = require('../../ipc');

const router = Router();
const MU = '/setting';

router.get(`${MU}/getData`, (req, res) => {
  (new Setting()).getData()
    .then(data => res.json(data))
    .catch(e => {
      res.sendStatus(500)
    });
});

router.post(`${MU}/save`, (req, res) => {
  let isRestartOS = req.query.hasOwnProperty('restartOS');
  (new Setting()).save(req.body, isRestartOS)
    .then(() => {
      res.sendStatus(201);
      Log.info('settings saved');
    })
    .catch(e => {
      res.send(500);
      Log.error(e);
    });
});

module.exports = router;

IPC.on('load-setting', (event, val) => {
  (new Setting())
    .getData()
    .then(data => {
      event.sender.send('load-setting', data);
    });
});

IPC.on('save-setting', (event, val) => {
  let isRestartOS = val.restartOS || false;
  (new Setting()).save(val, isRestartOS)
    .then(() => {
      event.sender.send('save-setting::ok');
    })
    .catch(e => {
      Log.error(e);
      event.sender.send('save-setting::error');
    });
});
