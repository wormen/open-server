const {Router} = require('express');
const path = require('path');
const Queue = require('../../mqtt');
const {getKey} = require('../../libs/os');
const Fs = require('../../libs/Fs');
const IPC = require('../../ipc');

const {nameItem, GenerateList} = require('../../libs/backup');

const router = Router();
const MU = '/backups';
const queue = new Queue();

router.get(`${MU}/getList`, (req, res) => {

  const onError = e => {
    console.error(e);
    res.sendStatus(500);
  };

  getList()
    .then(list => {
      res.send(list);
    })
    .catch(onError);
});

router.post(`${MU}/create`, (req, res) => {
  create(req.body)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/restore`, (req, res) => {
  restore(req.body)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/remove/:name`, (req, res) => {
  remove(req.params.name)
    .then(() => res.sendStatus(200))
    .catch(e => res.sendStatus(500));
});

module.exports = router;

IPC.on('backups::create', (event, val) => {
  create(val)
    .then(() => {
      event.sender.send('backups::create:ok');
    })
    .catch(e => {
      console.error(e);
      event.sender.send('backups::create:error');
    });
});

IPC.on('backups::restore', (event, val) => {
  restore(val)
    .then(() => {
      event.sender.send('backups::restore:ok');
    })
    .catch(e => {
      console.error(e);
      event.sender.send('backups::restore:error');
    });
});

IPC.on('backups::getList', (event, val) => {
  getList()
    .then(list => {
      event.sender.send('backups::getList:ok', list);
    })
    .catch(e => {
      console.error(e);
      event.sender.send('backups::getList:error');
    });
});

IPC.on('backups::remove', (event, name) => {
  remove(name)
    .then(() => {
      event.sender.send('backups::remove:ok');
    })
    .catch(e => {
      console.error(e);
      event.sender.send('backups::remove:error');
    });
});

function getList() {
  return new Promise((resolve, reject) => {
    getKey('sys').then(async (val) => {
      if (val && val.hasOwnProperty('backupDir')) {
        let dir = path.resolve(val.backupDir);

        return Fs.dirList(dir)
          .then(list => {
            const gl = new GenerateList();
            list.forEach(item => {
              gl.add(new nameItem(item).parse());
            });
            resolve(gl.get());
          });
      }
      reject(new Error('Invalid settings'));
    })
  });
}

function restore(data) {
  return queue.addToQueue(Queue.EVENTS.RESTORE_BACKUP, data)
}

function create(data) {
  return queue.addToQueue(Queue.EVENTS.BACKUP, data);
}

function remove(name) {
  return new Promise((resolve, reject) => {
    getKey('sys').then(async (val) => {
      if (val && val.hasOwnProperty('backupDir')) {
        let src = path.resolve(val.backupDir, name);
        return Fs.remove(src)
          .then(() => {
            resolve();
          })
          .catch(e => {
            reject(e);
          });
      }
      reject(new Error('Invalid settings'));
    })
  });
}
