const {Router} = require('express');
const MySQL = require('../../libs/os/MySQL');
const {exec} = require('child_process');
const path = require('path');
const IPC = require('../../ipc');

const router = Router();
const MU = '/mysql';

router.get(`${MU}/getList`, (req, res) => {
  (new MySQL()).getList()
    .then(list => res.json(list))
    .catch(e => res.sendStatus(500));
});

module.exports = router;

IPC.on('mysql::getList', (event, val) => {
  (new MySQL()).getList()
    .then(list => {
      event.sender.send('mysql::getList:ok', list);
    })
    .catch(e => {
      console.error(e);
      event.sender.send('mysql::getList:error');
    });
});
