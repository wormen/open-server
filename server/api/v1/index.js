/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {Router} = require('express');

const backups = require('./backups');
const domains = require('./domains');
const mysql = require('./mysql');
const email = require('./email');
const ftp = require('./ftp');
const ssh = require('./ssh');
const setting = require('./setting');

const router = Router();

router.get(`/test`, (req, res) => {
  res.json({
    ping: 'ok'
  });
});

router.use(backups);
router.use(domains);
router.use(mysql);
router.use(email);
router.use(ftp);
router.use(ssh);
router.use(setting);

module.exports = router;
