/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */
const deepmerge = require('deepmerge');

module.exports.isJson = function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

module.exports.isObject = function (arg) {
  return arg !== null && typeof arg === 'object';
};

module.exports.readCookie = function (cookie) {
  let c = cookie.split('; ');
  let obj = {};

  for (let line of c) {
    let str = line.split('=');
    obj[str[0]] = str[1]
  }

  return obj;
};

/**
 * Объединение объектов
 * @param origin
 * @param add
 * @param deep
 * @returns {*}
 */
module.exports.extend = function (origin, add, deep = false) {
  if (add === null || typeof add !== 'object') return origin;

  origin = deepmerge(origin, add);

  if (!deep) {
    return origin;
  } else {
    return JSON.parse(JSON.stringify(origin));
  }
}
