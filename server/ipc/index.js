/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://electronjs.org/docs/api/ipc-main
 */

const {ipcMain} = require('electron');
const {isJson} = require('../utils');

class IPC {
  _encode(val) {
  }

  _decode(val) {
    if (Array.isArray(val) || isJson(val)) {
      val = JSON.parse(val);
    }
    return val;
  }

  on(channel, listener = noop) {
    ipcMain.on(channel, (event, val) => {
      listener(event, this._decode(val));
    });
  }

  once(channel, listener = noop) {
    ipcMain.once(channel, (event, val) => {
      listener(event, this._decode(val));
    });
  }

  remove(channel, listener = noop) {
    ipcMain.removeListener(channel, listener);
  }
}

module.exports = new IPC();
