/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {existsSync} = require('fs');
const path = require('path');
const {spawn} = require('child_process');
const Fs = require('../libs/Fs');

class Index {
  /**
   * Запуск служебных сервисов
   * @returns {Promise}
   */
  static startServices(setting = {}) {
    return new Promise(async (resolve, reject) => {
      const dir = path.resolve($app.cwdOrig, 'workers'); // каталог, где лежат сервисы
      if (existsSync(dir)) {
        const list = await Fs.dirList(dir).catch(reject); // получаем список сервисов
        if (Array.isArray(list)) {
          list.forEach(item => {
            if (item.includes('.service')) {
              let sn = String(item).replace('.service.js', '');
              launchService(dir, item, setting[sn]);
            }
          });
        }
        resolve();
      } else {
        resolve();
      }
    });
  }
}

function launchService(dir, item, params) {
  let file = path.resolve(dir, item);
  let sv = require(file);
  if (typeof sv.setSetting === 'function') {
    sv.setSetting(params)
  }
}

module.exports = Index;
