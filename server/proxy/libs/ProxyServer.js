/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

// const _ = require('lodash');
const url = require('url');
const http = require('http');
// const httpConst = require('http-const');
// const _s = require('underscore.string');
const HttpProxy = require('./HttpProxy');
const HttpsProxy = require('./HttpsProxy');
const {EventEmitter} = require('events');
const Log = require('../../libs/Log');
const IPC = require('../../ipc');
const {readCookie} = require('../../utils');

let proxy = null;
let ignoreList = [];

// const boundaryRegexp = /boundary=-+\w+/im;
// const paramRegexp = /Content-Disposition:\s*form-data;\s*name="\w+"/im;
// const paramNameRegexp = /name="\w+"/im;
// const quoteRegexp = /"/g;

class ReqParser {
  constructor(protocol, req) {
    let _url = url.parse(req.url, true);

    this.protocol = protocol;
    this.host = _url.hostname;
    this.url = req.url;

    ['method', 'headers', 'httpVersion'].forEach(item => {
      this[item] = req[item];
    });

    this.query = _url.query;
    this.headers = req.headers;
    this.cookie = req.headers.hasOwnProperty('cookie') ? readCookie(req.headers.cookie) : {};

    // const contentTypeHeader = this._getContentTypeHeader(headers);
    this.body = req.body || {};
    // this._parseBody(req.body, contentTypeHeader);

    // if (protocol === 'http') {
    //   obj.response = res;
    // } else {
    //   obj = {...obj, ...opts};
    // }
  }
  //
  // _getContentTypeHeader(headers) {
  //   return headers['content-type'] || null;
  // }
  //
  // _parseBody(lines, contentTypeHeader) {
  //   if (!lines) {
  //     return null;
  //   }
  //
  //   let body = {};
  //
  //   if (!contentTypeHeader) {
  //     this._parsePlainBody(lines, body);
  //     return body;
  //   }
  //
  //   body.contentType = contentTypeHeader.value;
  //   switch (body.contentType) {
  //     case httpConst.contentTypes.formData:
  //       this._parseFormDataBody(lines, body, contentTypeHeader.params);
  //       break;
  //
  //     case httpConst.contentTypes.xWwwFormUrlencoded:
  //       this._parseXwwwFormUrlencodedBody(lines, body);
  //       break;
  //
  //     case httpConst.contentTypes.json:
  //       this._parseJsonBody(lines, body);
  //       break;
  //
  //     default:
  //       this._parsePlainBody(lines, body);
  //       break;
  //   }
  //
  //   return body;
  // }
  //
  // _parsePlainBody(lines, body) {
  //   body.plain = lines;
  // }
  //
  // _parseJsonBody(lines, body) {
  //   body.json = lines;
  // }
  //
  // _parseXwwwFormUrlencodedBody(lines, body) {
  //   let params = _s.words(lines, '&');
  //
  //   body.formDataParams = _(params).map(function (param) {
  //     let paramValue = _s.words(param, '=');
  //     if (paramValue.length !== 2) {
  //       throw new InvalidRequestError('Invalid x-www-form-url-encode parameter', param);
  //     }
  //
  //     return !paramValue[0] ? null : {
  //       name: paramValue[0],
  //       value: paramValue.length > 1 ? paramValue[1] : null
  //     };
  //   })
  //     .compact()
  //     .value();
  // }
  //
  // _parseFormDataBody(lines, body, contentTypeHeadeParams) {
  //   body.boundary = this.getBoundaryParameter(contentTypeHeadeParams);
  //
  //   let params = _s.words(lines, '-----------------------' + body.boundary);
  //
  //   body.formDataParams = _(params).map(function (param) {
  //     let paramMatch = param.match(paramRegexp);
  //     if (!paramMatch) {
  //       throw new InvalidRequestError('Invalid formData parameter', param);
  //     }
  //
  //     let paramNameMatch = paramMatch.toString().match(paramNameRegexp); // TODO: refactor to remove toString
  //     if (!paramNameMatch) {
  //       throw new InvalidRequestError('formData parameter name must have format: [Name]="[Value]"', param);
  //     }
  //
  //     let paramNameParts = _s.words(paramNameMatch, '=');
  //     if (paramNameParts.length !== 2) {
  //       throw new InvalidRequestError('formData parameter name must have format: [Name]="[Value]"', param);
  //     }
  //     let paramName = paramNameParts[1];
  //     let paramValue = param.replace(paramMatch, '').trim('\n');
  //
  //     return {
  //       name: paramName.toString().replace(quoteRegexp, ''), // TODO: refactor to remove toString
  //       value: paramValue
  //     };
  //   })
  //     .value();
  // }
  //
  // getBoundaryParameter(contentTypeHeaderParams) {
  //   if (!contentTypeHeaderParams) {
  //     throw new InvalidRequestError('Request with ContentType=FormData must have a header with boundary');
  //   }
  //
  //   let boundaryMatch = contentTypeHeaderParams.match(boundaryRegexp);
  //   if (!boundaryMatch) {
  //     throw new InvalidRequestError('Boundary param must have format: [boundary]=[value]', contentTypeHeaderParams);
  //   }
  //
  //   let boundaryAndValue = _s.words(boundaryMatch, '=');
  //   if (boundaryAndValue.length !== 2) {
  //     throw new InvalidRequestError('Boundary param must have format: [boundary]=[value]', contentTypeHeaderParams);
  //   }
  //
  //   let boundaryValue = _s.trim(boundaryAndValue[1]);
  //   if (!boundaryValue) {
  //     throw new InvalidRequestError('Boundary param must have format: [boundary]=[value]', contentTypeHeaderParams);
  //   }
  //   return boundaryValue;
  // }
}

class ProxyServer extends EventEmitter {
  constructor(opts = {}) {
    super();
    this._opts = opts;
    this.port = opts.port || 8080;
  }

  _reqLog(protocol, req, res, opts = {}) {
    this.emit('request', new ReqParser(protocol, req));
  }

  _isIgnore(url) {
    let isIgnore = false;
    for (let line of ignoreList) {
      if (String(url).includes(line)) {
        isIgnore = true;
        break;
      }
    }
    return isIgnore;
  }

  createServer(done) {
    // HTTP
    proxy = http.createServer((request, response) => {
      try {
        this.emit('replace:url', request.url, (url) => {
          request.url = url;
        });

        if (!this._isIgnore(request.url)) {
          this._reqLog('http', request, response);
        }
        new HttpProxy(request, response);
      } catch (e) {
      }
    });

    // HTTPS
    proxy.on('connect', (request, socketRequest, head) => {
      try {
        this.emit('replace:url', request.url, (url) => {
          request.url = url;
        });

        if (!this._isIgnore(request.url)) {
          this._reqLog('https', request, null, {
            socketRequest,
            head
          });
        }
        new HttpsProxy(request, socketRequest, head);
      } catch (e) {
      }
    });

    proxy.listen(this.port, () => {
      Log.info('Proxy Server listening on 0.0.0.0:' + this.port);
      if (typeof done === 'function') {
        done();
      }
    });

    return proxy;
  }

  restart(opts, done) {
    this._opts = Object.assign(this._opts, opts || {});
    if (opts && opts.port) {
      this.port = opts.port;
    }

    try {
      proxy.close();
      proxy = null;
      this.createServer(() => {
        if (typeof done === 'function') {
          done();
        }
      });
    } catch (e) {
      console.error(e);
    }
  }

  setIgnore(list) {
    ignoreList = [].concat(ignoreList, list);
  }
}

module.exports = module.exports['default'] = ProxyServer;

IPC.on('proxy::ignore', (event, val) => {
  if (Array.isArray(val)) {
    ignoreList = [].concat(ignoreList, val);
  }
});

function InvalidRequestError(message, data) {
  if (message) {
    this.message = data ?
      _s.sprintf('Invalid request message. %s. Data: %s', message, data) :
      _s.sprintf('Invalid request message. %s', message);
  } else {
    this.message = 'Invalid request message.';
  }
}
