/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const net = require('net');
const {parse} = require('url');
const {EventEmitter} = require('events');

class HttpsProxy extends EventEmitter {
  constructor(request, socketRequest, head) {
    super();

    try {
      this._log(request.url);

      const ph = this._urlParse('http://' + request.url);

      const socket = net.connect(ph.port, ph.hostname, () => {
        socket.write(head);

        // Сказать клиенту, что соединение установлено
        socketRequest.write(`HTTP/${request.httpVersion} 200 Connection established\r\n\r\n`)
      });

      // Туннелирование к хосту
      socket.on('data', (chunk) => {
        socketRequest.write(chunk)
      });

      socket.on('end', () => {
        socketRequest.end()
      });

      socket.on('error', () => {
        // Сказать клиенту, что произошла ошибка
        socketRequest.write(`HTTP/${request.httpVersion} 500 Connection error\r\n\r\n`);
        socketRequest.end()
      });

      // Туннелирование к клиенту
      socketRequest.on('data', (chunk) => {
        socket.write(chunk)
      });

      socketRequest.on('end', () => {
        socket.end()
      });

      socketRequest.on('error', () => {
        socket.end()
      });
    } catch (e) {
    }
  }

  _log(...args) {
    // this.emit('log', '[Proxy HTTPS]', ...args);
  }

  _urlParse(url) {
    return parse(url);
  }
}

module.exports = HttpsProxy;
