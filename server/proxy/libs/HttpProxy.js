/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const http = require('http');
const {parse} = require('url');
const {EventEmitter} = require('events');

class HttpProxy extends EventEmitter {
  constructor(request, response) {
    super();

    let options = this._getOptions(this._urlParse(request.url), request);

    this._proxyRequest(request, response, options);
  }

  _getOptions(ph, req) {
    return {
      port: ph.port,
      hostname: ph.hostname,
      method: req.method,
      path: ph.path,
      headers: req.headers
    }
  }

  _urlParse(url) {
    return parse(url);
  }

  _proxyRequest(req, res, options) {
    const proxyRequest = http.request(options);

    proxyRequest.on('response', (proxyResponse) => {
      proxyResponse.on('data', (chunk) => {
        res.write(chunk, 'binary');
      });

      proxyResponse.on('end', () => {
        res.end()
      });

      res.writeHead(proxyResponse.statusCode, proxyResponse.headers)
    });

    req.on('data', (chunk) => {
      proxyRequest.write(chunk, 'binary')
    });

    req.on('end', () => {
      proxyRequest.end()
    });
  }
}

module.exports = HttpProxy;
