/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const {EventEmitter} = require('events');
const path = require('path');
const pm2 = require('pm2');

const noop = () => {
};

class PM2 extends EventEmitter {
  constructor() {
    super();

    this._noDaemonMode = true;
    this._monitInterval = 500;
  }

  Error(e) {
    this.emit('error', e);
  }

  _pub(type, data) {
    data.type = type;
    if (data.data) {
      data.data = data.data.trim();
    }
    super.emit('log', data);
  }

  _monit() {
    this.list();
    setTimeout(() => {
      this._monit();
    }, this._monitInterval);
  }

  list(done = noop) {
    pm2.list((err, processDescriptionList) => {
      let arr = [];
      if (err) {
        return done(null, arr);
      }

      processDescriptionList.forEach(item => {
        arr.push(Object.assign({
          pid: item.pid,
          name: item.name,
          pm_id: item.pm_id,
          status: item.pm2_env.status,
          exec_mode: item.pm2_env.exec_mode,
          restarts: item.pm2_env.restart_time,
          created: item.pm2_env.created_at
        }, item.monit));
      });
      super.emit('monit', arr);
      done(null, processDescriptionList);
    });
  }

  start(appObj = {}, cb = noop) {
    pm2.start(appObj, cb);
  }

  stop(pmId, done = noop) {
    pm2.stop(pmId, done);
  }

  restart(pmId, done = noop) {
    pm2.restart(pmId, done);
  }

  delete(pmId, done = noop) {
    pm2.delete(pmId, done);
  }

  reload(pmId, done = noop) {
    pm2.reload(pmId, done);
  }

  describe(pmId, done = noop) {
    pm2.describe(pmId, done);
  }

  /**
   * @param platform - ubuntu | centos | redhat | gentoo | systemd | darwin | amazon
   * @param done
   */
  startup(platform, done = noop) {
    pm2.startup(platform, done)
  }

  disconnect() {
    pm2.disconnect();
  }

  connect(daemonMode = this._noDaemonMode) {
    pm2.connect(daemonMode, (error) => {
      if (error) {
        this.Error(error);
        return;
      }

      super.emit('ready');
    });

    pm2.launchBus((err, bus) => {
      if (err) {
        super.Error(err);
      }
      bus.on('log:out', data => {
        this._pub('out', data);
      });

      bus.on('log:err', data => {
        this._pub('err', data);
      });
    });

    this._monit();
  }

  services() {
    const apps = [
      {
        name: 'OS Service | Backup',
        cwd: ROOT_DIR,
        script: path.resolve(ROOT_DIR, 'workers', 'backup.service.js'),

        log_date_format: 'YYYY-MM-DD HH:mm:ss',
        error_file: resolve(LOGS_DIR, 'backup_error.log'),
        out_file: resolve(LOGS_DIR, 'backup_out.log'),
        watch: false,
        ignore_watch: [
          'node_modules'
        ],

        exec_interpreter: 'node',
        exec_mode: 'fork'
      },
      {
        name: 'OS Service | Logger',
        cwd: ROOT_DIR,
        script: path.resolve(ROOT_DIR, 'workers', 'log.service.js'),

        log_date_format: 'YYYY-MM-DD HH:mm:ss',
        error_file: resolve(LOGS_DIR, 'log_error.log'),
        out_file: resolve(LOGS_DIR, 'log_out.log'),
        watch: false,
        ignore_watch: [
          'node_modules'
        ],

        exec_interpreter: 'node',
        exec_mode: 'fork'
      }
    ];
    // todo зачитваем каталог с сервисами
    apps.forEach(app => {
      this.start(app);
    });
  }
}

module.exports = PM2;
